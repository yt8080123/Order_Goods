require(['jquery',
        'global',
        'module/util',
        'bootstrap',
        'validateMethod'],
    function($, global, util){
        $('#userMessageForm').validateHandler({
            rules: {
                userName: {
                    required: true
                },
                userLoginName: {
                    required: true
                },
                userPhone: {
                    required: true,
                    isMobile: 'required'
                },
                userIdcard:{
                    isIdCardNo:'required'
                },
                userType:{
                    required: true
                }
            },
            messages: {
                userName: {
                    required: "用户名不能为空"
                },
                userLoginName: {
                    required: "登录名不能为空"
                },
                userPhone: {
                    required: "手机号不能为空",
                    isMobile:"请输入正确手机号码"
                },
                userIdcard: {
                    required: "身份证号码不能为空"
                }
            },
            submitHandler: function (form) {
                util.ajax_submit("#userMessageForm").complete(function(xhr) {
                    var result = $.parseJSON(xhr.responseText);
                    if(result.code=="ACK"){
                        util.closeDialog();
                        $("#onSearch").click();
                    }
                });
            }

        });
    })
