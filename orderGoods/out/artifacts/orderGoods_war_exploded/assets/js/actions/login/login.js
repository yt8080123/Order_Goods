/**
 * Created by Administrator on 2017/1/24 0024.
 */

require(['jquery',
        'global',
        'bootstrap'],
function($, global){

    var init = function(){
        console.log("==================================");
    }

    init();

    $("#loginBtn").click(function () {
        var userName = $("input[name='userName']").val();
        var userPassword = $("input[name='userPassword']").val();
        var obj = {userName : userName,userPassword:userPassword};
        $.ajax({
            url:global.context+"/auth/doLogin",
            data:JSON.stringify(obj),
            datatype : "json",
            type:"POST",
            contentType:"application/json;charset=utf-8",
            success:function(xhr){
                console.log("success===");
            },
            error:function (xhr) {
                console.log("error===");
            }
        });
    });
});