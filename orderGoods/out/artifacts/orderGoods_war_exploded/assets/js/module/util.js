define(
		[ 'jquery' ],
		function($) {

			var Util = function() {
			};

			Util.prototype = {

				stringify_aoData : function(aoData) {
					var o = {};
					var modifiers = [ 'mDataProp_', 'sSearch_', 'iSortCol_',
							'bSortable_', 'bRegex_', 'bSearchable_',
							'sSortDir_' ];
					$.each(aoData,function(idx, obj) {
						if (obj.name) {
							for (var i = 0; i < modifiers.length; i++) {
								if (obj.name.substring(0,modifiers[i].length) == modifiers[i]) {
									var index = parseInt(obj.name.substring(modifiers[i].length));
									var key = 'a'+ modifiers[i].substring(0,modifiers[i].length - 1);
									if (!o[key]) {
										o[key] = [];
									}
									o[key][index] = obj.value;
									return;
								}
							}
							o[obj.name] = obj.value;
						} else {
							o[idx] = obj;
						}
					});
					return JSON.stringify(o);
				},

				set_menu : function(item) {
					$('.navbar-fixed-top .navbar-nav li').removeClass('active');
					$('.navbar-fixed-top .navbar-nav li').each(function(i) {
						var menuItem = $(this).attr('data-menu-item');
						if (menuItem == item) {
							$(this).addClass('active');
						}
					});
				},
				
				date_to_string:function(date){
					var res= '';
					try {
						var d = eval('new Date(' + date.toString().replace(/\d+(?=-[^-]+$)/,function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
						res=d.getFullYear()+"-"+("0"+(d.getMonth()+1)).slice(-2)+"-"+("0"+d.getDate()).slice(-2);  //  调整日期格式：yyyy-MM-dd
					} catch (e) {
						return res;
					}
					return res;
				},
				
				rise_to_color:function(rise){
					if(rise < 0){
						return "down";
					}else if(rise > 0){
						return "up";
					}
					return "zero";
				},
				ajax_submit : function(form) {
					var o = {};
					$(form).find('input,textarea,select').each(function() {
						if( $(this).attr('name')!='multiple'){
						var key = $(this).attr('name');
						if (key) {
							o[key] = $(this).val();
						}
						}
					});
					return $.ajax({
						url : $(form).attr('action'),
						type : $(form).attr('method'),
						dataType : 'json',
						headers : {
							'x-form-id' : $(form).attr('id')
						},
						contentType : 'application/json; charset=UTF-8',
						data : JSON.stringify(o)
					});
				},

				redirect : function(url) {
					// Similar behavior as an HTTP redirect
					window.location.replace(url);
				},

				getSearchData : function(containerId) {
					var result = [];
					$(containerId)
							.find('input,textarea,select')
							.each(
									function() {
										var o = {};
										var key;
										if ($(this).attr("data-ignore") == "true") {
											return true;
										}
										if ($(this)
												.hasClass(
														"select2-focusser select2-offscreen")
												|| $(this).hasClass(
														"select2-input")) {
											return true;
										}
										key = $(this).attr('name');
										if (key) {
											if ($(this).attr("Type") == "checkbox") {
												o["name"] = key;
												if ($(this).val() == "true") {
													o["value"] = true;
												} else {
													o["value"] = false;
												}
											} else if ($(this).attr("Type") == "radio") {
												if ($(this).is(":checked")) {
													o["name"] = key;
													o["value"] = $(this).val();
												} else {
													return;
												}
											} else {
												o["name"] = key;
												o["value"] = $(this).val();
											}
											result.push(o);
										}
									});
					return result;
				},
				
				updatePagerIcons : function updatePagerIcons(table) {
					var replacement = 
					{
						'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
						'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
						'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
						'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
					};
					$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
						var icon = $(this);
						var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
						
						if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
					})
				},
				
				enableTooltips:function enableTooltips(table) {
					$('.navtable .ui-pg-button').tooltip({container:'body'});
					$(table).find('.ui-pg-div').tooltip({container:'body'});
				},
				
				getFormJson : function getFormJson(formId){
					var o = {};
					var form=$(formId);
					$(form).find('input,textarea,select').each(function() {
						var key = $(this).attr('name');
						if (key) {
							if($(this).attr('type')=='radio'){
								if($(this).attr('checked')){
									o[key] = $(this).val();
								}
							}else{
								o[key] = $(this).val();
							}
						}
					});
					return o;
				},
				
				submitFormByAjax:function submitFormByAjax(formId,url,options,type){
					var o = {};
					var form=$(formId);
					$(form).find('input,textarea,select').each(function() {
						var key = $(this).attr('name');
						if (key) {
							if($(this).attr('type')=='radio'){
								if($(this).attr('checked')){
									o[key] = $(this).val();
								}
							}else{
								o[key] = $(this).val();
							}
						}
					});
					$.ajax({
						   type: type,
						   url: url,
						   data: JSON.stringify(o),
						   contentType: "application/json;charset=UTF-8",
						   success: options.success
					});
				},

				openDialog:function  openDialog(options) {
					this.clearDialog();
					var option = $.extend(dialogDefaul,options);
                    $('.modal-body').load(option.url);
                    $(".modal-title").html(option.title);
                    $("#dialogSize").addClass(option.class);
                    $('#dialogId').modal({backdrop: 'static', keyboard: false});
                },

				clearDialog:function clearDialog() {
                    $('.modal-body').html("");
                    $(".modal-title").html("");
                },

				closeDialog:function closeDialog() {
                    $('.modal-body').html("");
                    $('#dialogId').modal('hide');
                    $('.modal-backdrop').remove();
                }
				
			};

			var dialogDefaul = {
				url:"",
				class:"modal-lg",
				title:"标题"
			};

			return new Util();
		});