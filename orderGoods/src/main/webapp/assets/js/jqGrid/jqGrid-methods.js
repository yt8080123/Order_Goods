/*******************************************************************************
 * jQuery Validate扩展验证方法
 ******************************************************************************/
define(
		[ 'jquery'],
		function($) {
	// 用户选择插件
	$.fn.jqGridShow = function(options) {
		var tableId = $(this).attr("id");
		// build main options before element iteration
		
    	adjustWidth();
    	$(window).resize(function(){
    		if (parseInt($("#sidebar").css("height")) === 1) {
    			adjustWidth();
    			$("#"+tableId).setGridWidth(document.body.clientWidth - 40);
    		} else {
    			adjustWidth();
    			$("#"+tableId).setGridWidth(document.body.clientWidth - parseInt($("#sidebar").css("width")) - 40);
    		}
    		
    	});
		
		var opts = $.extend({}, $.fn.jqGridShow.defaults, options);
		if (opts.debug == true) {
			debugger;
		}
		$(this).jqGrid({
			contentType: "application/json",
	        datatype : "json",
	        rowNum : opts.rowNum,
	        height: opts.height,
	        autowidth: true,
	        shrinkToFit: true,
	        scrollrows: false,
	        multiselect: opts.multiselect,
	        multiboxonly: true,
	        rowList : opts.rowList,
	        mtype : "post",
	        viewrecords : true,
	        sortorder : "desc",
	        sortable:false,
			url:opts.url,
			postData:opts.postData,
			colNames:opts.colNames,
			colModel:opts.colModel,
			caption:opts.caption,
			pager:opts.pager,
			onSelectRow:opts.onSelectRow,
			onSelectAll:opts.onSelectAll,
			onCellSelect:opts.onCellSelect,
			gridComplete:opts.gridComplete,
			loadComplete : function() {
				var table = this;
				setTimeout(function(){
					var replacement = 
					{
						'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
						'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
						'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
						'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
					};
					$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
						var icon = $(this);
						var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
						
						if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
					})
				}, 0);
				
				if(!(typeof(opts.myCompelete)=="undefined")){
					var myCompelete = opts.myCompelete;
					myCompelete();
				}
			}
		});
		$(this).closest(".ui-jqgrid-bdiv").css({ 'overflow-y' : 'scroll' }); 
	};
	
	// 用户插件的defaults
	$.fn.jqGridShow.defaults = {
			contentType: "application/json",
			datatype : "json",
			rowNum : 20,
			height: 540,
			autowidth: true,
			multiselect: false,
			multiboxonly: true,
			rowList : [ 20, 40, 60 ],
			mtype : "post",
			viewrecords : true,
			sortorder : "desc",
			sortable:false
	};


});

function adjustWidth() {
	var rightWidth = document.body.clientWidth - parseInt($("#sidebar").css("width"));
	$(".main-content").css("width", rightWidth);
};