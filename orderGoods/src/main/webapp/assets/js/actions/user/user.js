require(['jquery',
        'global',
        'module/util',
        'bootstrap',
        'jqGridMethod'],
    function($, global, util){

        var pageInit = function pageInit(){
            //激活菜单
            $('#01').addClass("active open hsub");
            $('#0101').addClass("active");

            $("input[name='userName']").focus();
            view();
        };

        var view = function view(){
            var grid_selector = "#grid-table";
            var pager_selector = "#grid-pager";
            var o = {};
            var userName = $("#userName").val();

            $(grid_selector).jqGridShow(
                {
                    url : global.context+'/userInfo/getUserInfoList',
                    postData : o,
                    colNames : [ 'Id','用户名', '用户登录名', '电话号码', '身份证号码','微信','支付宝账号','注册日期' ,'用户类型','操作'],
                    colModel : [
                        {name : 'userId',index : 'userId',width : 115,sortable: false,hidden:true},
                        {name : 'userName',index : 'userName',width : 30,sortable: false},
                        {name : 'userLoginName',index : 'userLoginName',width : 30,sortable: false},
                        {name : 'userPhone',index : 'userPhone',width : 50,sortable: false},
                        {name : 'userIdcard',index : 'userIdcard',width : 60,sortable: false},
                        {name : 'userWechat',index : 'userWechat',width : 50,sortable: false},
                        {name : 'userAlipay',index : 'userAlipay',width : 50,sortable: false},
                        {name : 'createTime',index : 'createTime',width : 80,sortable: false},
                        {name : 'userType',index : 'userType',width : 80,sortable: false},
                        {name:'Modify',index:'Id',width:60,sortable:false}
                    ],
                    pager : pager_selector,
                    autowidth:true,
                    //sortname : 'userName',
                    caption : "用户信息管理",
                    gridComplete:function(){  //在此事件中循环为每一行添加修改和删除链接
                        var ids=jQuery(grid_selector).jqGrid('getDataIDs');
                        for(var i=0; i<ids.length; i++){
                            var id=ids[i];
                            var model = jQuery("#grid-table").jqGrid('getRowData', id);
                            modify = "<a title='编辑' class='blue editUser' href='#' style='color:#f60' id='"+model.userId+"' >编辑</a>" ;
                            //这里的onclick就是调用了上面的javascript函数 Modify(id)
                            jQuery(grid_selector).jqGrid('setRowData', ids[i], { Modify: modify });

                        }
                    }
                });
        }


        $("#onSearch").click(function(){
            var o = util.getFormJson("#onSearchForm");
            jQuery("#grid-table").jqGrid('setGridParam',
                { url : global.context+'/userInfo/getUserInfoList',
                    page : 1 ,
                    postData : o}).trigger("reloadGrid");
        });

        $("body").on("click",".editUser",function () {
            var id = this.id;
            console.log(id);
            util.openDialog({url:"../userInfo/openEditUser?userId="+id,title:"编辑用户信息"});
        })

        var init = function(){
            pageInit();
        }

        $("#addUserInfo").click(function () {
            console.log("addUserinfo");
            util.openDialog({url:"../userInfo/openAddUserPage"});
        });

        init();

    })


function Modify(id) {   //单击修改链接的操作
    var model = jQuery("#grid-table").jqGrid('getRowData', id);
    $('.modal-body').load("editUserInfo?userName=" + model.userName);
    $(".modal-title").html("修改用户信息");
    $('#myModal').modal({backdrop: 'static', keyboard: false});
};

function passWord(id){
    var model = jQuery("#grid-table").jqGrid('getRowData', id);
    $(".modal-title").html("重置密码");
    $('.modal-body').load("showUserInfo?userName=" + model.userName);
    $('#myModal').modal({backdrop: 'static', keyboard: false});

};

function ModifyDelete(id) {   //单击删除链接的操作
    var model = jQuery("#grid-table").jqGrid('getRowData', id);
    $('.modal-body').load("deleteUserInfo?userName=" + model.userName);
    //            $("#username").val(model.userName);
    $('#myModal').modal({backdrop: 'static', keyboard: false});
    $(".modal-title").html("删除用户信息");
};

