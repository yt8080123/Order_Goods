/*******************************************************************************
 * jQuery Validate扩展验证方法
 ******************************************************************************/
define(
		[ 'jquery'],
		function($) {
	// 用户选择插件
	$.prototype = function openDialog(options) {

		var opts = $.extend({}, defaults, options);
        $('.modal-body').load(opts.url);
        $(".modal-title").html(opts.title);
        $("#dialogSize").addClass(opts.class);
        $('#myModal').modal({backdrop: 'static', keyboard: false});
	};

	// 用户插件的defaults
	var defaults = {
			url:"",
			title:"标题",
			class:"modal-lg"
	};


});