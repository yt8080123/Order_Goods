/**
 * Created by Administrator on 2017/1/24 0024.
 */

require(['jquery',
        'global',
        'module/util',
        'bootstrap',
        'metisMenu',
        'slimscroll',
        'layer',
        'hplus',
        'contabs',
        'pace'],
    function($, global, util){

        $("#editSelf").click(function () {
            util.openDialog({url:global.context+"/userInfo/openEditUser?userId=",title:"编辑个人信息"});
        });
    });
