require(['global'], function(global){
	require.config({
//		urlArgs: "bust=" + (new Date()).getTime(),
		baseUrl: global.context + '/assets/js',
		paths : {
			// the left side is the module ID, the right side is the path to
			// the jQuery file, relative to baseUrl. Also, the path should NOT include
			// the '.js' file extension. This example is using jQuery located at
			// vendor/jquery/jquery-1.11.0.js, relative to the HTML page.
			'jquery' : 'jquery/jquery.min',
            'bootstrap':'bootstrap/bootstrap.min',
            'metisMenu':'metisMenu/jquery.metisMenu',
            'slimscroll':'slimscroll/jquery.slimscroll.min',
            'layer':'layer/layer.min',
            'hplus':'hplus/hplus',
            'contabs':'contabs',
            'pace':'pace/pace.min',
            'gridLocale':'jqGrid/js/i18n/grid.locale-cn',
            'jqGrid':'jqGrid/js/jquery.jqGrid.min',
            'jqGridMethod':'jqGrid/jqGrid-methods',
            'Util':'module/util',
            'jq-validate':'validate/jquery.validate',
            'validateMethod':'validate/validate-methods',
            'address':'module/jsAddress',
            'WdatePicker':'WdatePicker/WdatePicker',
            'searchSelect':'searchSelect/jquery.searchableSelect',
            'jsonpJquery':'jquery/jquery.jsonp'
        },
        // Remember: only use shim config for non-AMD scripts,
		// scripts that do not already call define(). The shim
		// config will not work correctly if used on AMD scripts,
		// in particular, the exports and init config will not
		// be triggered, and the deps config will be confusing
		// for those cases.
		shim : {
			'bootstrap':{deps : [ 'jquery' ]},
            'metisMenu':{deps : [ 'jquery' ]},
            'slimscroll':{deps : [ 'jquery' ]},
            'layer':{deps : [ 'jquery' ]},
            'hplus':{deps : [ 'jquery' ]},
            'contabs':{deps : [ 'jquery' ]},
            'pace':{deps : [ 'jquery' ]},
            'jqGrid':{deps: [ 'jquery' ]},
            'gridLocale':{deps: [ 'jqGrid' ]},
            'jqGridMethod':{deps: [ 'gridLocale' ]},
            'jq-validate':{deps: ['jquery']},
            'validateMethod':{deps: ['jq-validate']},
            'address':{deps:['jquery']},
            'WdatePicker':{deps:['jquery']},
            'searchSelect':{deps:['jquery']},
            'jsonpJquery':{deps:['jquery']}
		}
	});
});
