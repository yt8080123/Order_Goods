package test;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class CopyFile {

    private static final List<XMGLTaskDTO> xmgList = new ArrayList<XMGLTaskDTO>();

    static {
        XMGLTaskDTO xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 1L;
        xmglTaskDTO.parentId = 0L;
        xmglTaskDTO.name = "10";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 2L;
        xmglTaskDTO.parentId = 1L;
        xmglTaskDTO.name = "21";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 3L;
        xmglTaskDTO.parentId = 1L;
        xmglTaskDTO.name = "31";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 4L;
        xmglTaskDTO.parentId = 2L;
        xmglTaskDTO.name = "42";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 5L;
        xmglTaskDTO.parentId = 3L;
        xmglTaskDTO.name = "53";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 6L;
        xmglTaskDTO.parentId = 0L;
        xmglTaskDTO.name = "60";
        xmgList.add(xmglTaskDTO);
        xmglTaskDTO = new XMGLTaskDTO();
        xmglTaskDTO.id = 7L;
        xmglTaskDTO.parentId = 4L;
        xmglTaskDTO.name = "74";
        xmgList.add(xmglTaskDTO);
    }
    public static void main(String[] args){
        for(XMGLTaskDTO xmglTaskDTO : xmgList){
            System.out.println("id:"+xmglTaskDTO.id+";parentId:"+xmglTaskDTO.parentId+";name:"+xmglTaskDTO.name);
        }
        List<XMGLTaskDTO > nodeList = new ArrayList();
        for(XMGLTaskDTO node1 : xmgList){//taskDTOList 是数据库获取的List列表数据或者来自其他数据源的List
            boolean mark = false;
            for(XMGLTaskDTO node2 : xmgList){
                if(node1.parentId!=null && node1.parentId == node2.id){
                    mark = true;
                    if(node2.childrenTaskList == null)
                        node2.childrenTaskList = new ArrayList<XMGLTaskDTO>();
                    node2.childrenTaskList.add(node1);
                    break;
                }
            }
            if(!mark){
                nodeList.add(node1);
            }
        }
        System.out.println("=========================");
        for(XMGLTaskDTO xmglTaskDTO : nodeList){
            System.out.println("id:"+xmglTaskDTO.id+";parentId:"+xmglTaskDTO.parentId+";name:"+xmglTaskDTO.name);
            printAll(xmglTaskDTO.childrenTaskList,xmglTaskDTO.name,1);
        }
    }

    public static void printAll(List<XMGLTaskDTO> list,String parentName,int num){
        if(list!=null && list.size()>0){
            for(XMGLTaskDTO xmglTaskDTO : list){
                String ss = "";
                for(int i = 0;i<num ;i++){
                    ss += "--";
                }
                System.out.println(ss+"id:"+xmglTaskDTO.id+";parentId:"+xmglTaskDTO.parentId+";name:"+xmglTaskDTO.name);
                printAll(xmglTaskDTO.childrenTaskList,xmglTaskDTO.name,num+1);
            }
        }
    }

}

class XMGLTaskDTO {
    String name;
    Long parentId;
    Long id;
    List<XMGLTaskDTO > childrenTaskList;

}
