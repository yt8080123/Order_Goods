package com.hzyzt.user.dto;

import com.common.dto.PageInfoDto;

public class UserDto extends PageInfoDto{
    private Integer userId;

    private String userName;

    private String userLoginName;

    private String userPassword;

    private String userPhone;

    private String userIdcard;

    private String userWechat;

    private String userAlipay;

    private String createTime;

    private String userType;

    /**
     * @serialField : 站点id
     */
    private Integer siteId;

    private String siteCommName;

    public UserDto(String userLoginName){
        this.userLoginName = userLoginName;
    }

    public UserDto(String userLoginName,String userName){
        this.userLoginName = userLoginName;
        this.userName = userName;
    }

    public UserDto(){}

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName == null ? null : userLoginName.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone == null ? null : userPhone.trim();
    }

    public String getUserIdcard() {
        return userIdcard;
    }

    public void setUserIdcard(String userIdcard) {
        this.userIdcard = userIdcard == null ? null : userIdcard.trim();
    }

    public String getUserWechat() {
        return userWechat;
    }

    public void setUserWechat(String userWechat) {
        this.userWechat = userWechat == null ? null : userWechat.trim();
    }

    public String getUserAlipay() {
        return userAlipay;
    }

    public void setUserAlipay(String userAlipay) {
        this.userAlipay = userAlipay == null ? null : userAlipay.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public String getSiteCommName() {
        return siteCommName;
    }

    public void setSiteCommName(String siteCommName) {
        this.siteCommName = siteCommName;
    }
}