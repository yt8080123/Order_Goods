package com.hzyzt.user.dto;

import java.util.List;

public class MenuInfoDto {
    private String menuId;

    private String menuName;

    private String menuLevel;

    private String menuUrl;

    private String parentMenu;

    private Integer menuOrder;

    private String menuIcon;

    private String menuPermission;

    private List<MenuInfoDto> menuInfoDtoList;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId == null ? null : menuId.trim();
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public String getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(String menuLevel) {
        this.menuLevel = menuLevel == null ? null : menuLevel.trim();
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl == null ? null : menuUrl.trim();
    }

    public String getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(String parentMenu) {
        this.parentMenu = parentMenu == null ? null : parentMenu.trim();
    }

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon == null ? null : menuIcon.trim();
    }

    public String getMenuPermission() {
        return menuPermission;
    }

    public void setMenuPermission(String menuPermission) {
        this.menuPermission = menuPermission == null ? null : menuPermission.trim();
    }

    public List<MenuInfoDto> getMenuInfoDtoList() {
        return menuInfoDtoList;
    }

    public void setMenuInfoDtoList(List<MenuInfoDto> menuInfoDtoList) {
        this.menuInfoDtoList = menuInfoDtoList;
    }
}