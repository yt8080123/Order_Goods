package com.hzyzt.user.service;

import com.hzyzt.user.dto.MenuInfoDto;

import java.util.List;

/**
 * @Description :
 * @Author : 吴书新
 */
public interface MenuInfoService {

    /**
     * @Description : 得到全部的菜单
     */
    public List<MenuInfoDto> getAllMenu() throws Exception;

}
