package com.hzyzt.user.service.impl;

import com.common.util.WebUtil;
import com.hzyzt.user.dao.MenuInfoMapper;
import com.hzyzt.user.dao.UserPermissionMapper;
import com.hzyzt.user.dto.MenuInfoDto;
import com.hzyzt.user.service.MenuInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description :
 * @Author : 吴书新
 */
@Service
@Transactional
public class MenuInfoServiceImpl implements MenuInfoService{
    private static final Logger LOGGER = LoggerFactory.getLogger(MenuInfoServiceImpl.class);

    @Autowired
    private MenuInfoMapper menuInfoMapper;
    @Autowired
    private UserPermissionMapper userPermissionMapper;

    /**
     * 获取用户权限.
     */
    private List<String> getUserPermission() throws Exception{
        String userId = WebUtil.getSessionUserId();
        return userPermissionMapper.getUserPermission(userId);
    }

    /**
     * 得到全部的菜单
     */
    @Override
    public List<MenuInfoDto> getAllMenu() throws Exception {
        List<String> prems = getUserPermission();
        List<MenuInfoDto> menuInfoDtos = menuInfoMapper.getAllMenu(prems);
        List<MenuInfoDto> sysMenuAllDtoList = new ArrayList<MenuInfoDto>();
        List<MenuInfoDto> sysMenuTwoDtoListAdd = null;
        MenuInfoDto sysMenuDtoOne = null;
        MenuInfoDto sysMenuDtoTwo = null;
        // 获取一级菜单
        List<MenuInfoDto> sysMenuOneDtoList = findSysMenuDtoOne(menuInfoDtos);

        for (int i = 0; i < sysMenuOneDtoList.size(); i++) {
            sysMenuTwoDtoListAdd = new ArrayList<MenuInfoDto>();
            sysMenuDtoOne = new MenuInfoDto();
            sysMenuDtoOne.setMenuId(sysMenuOneDtoList.get(i).getMenuId());
            sysMenuDtoOne.setMenuName(sysMenuOneDtoList.get(i).getMenuName());
            sysMenuDtoOne.setMenuIcon(sysMenuOneDtoList.get(i).getMenuIcon());
            sysMenuDtoOne.setMenuUrl(sysMenuOneDtoList.get(i).getMenuUrl());
            sysMenuDtoOne.setMenuPermission(sysMenuOneDtoList.get(i)
                    .getMenuPermission());
            // 获取一级菜单下的二级菜单
            List<MenuInfoDto> sysMenuTwoDtoList = findSysMenuDtoTwo(
                    menuInfoDtos, sysMenuOneDtoList, i);

            for (int k = 0; k < sysMenuTwoDtoList.size(); k++) {
                sysMenuDtoTwo = new MenuInfoDto();
                sysMenuDtoTwo.setMenuId(sysMenuTwoDtoList.get(k).getMenuId());
                sysMenuDtoTwo.setMenuName(sysMenuTwoDtoList.get(k)
                        .getMenuName());
                sysMenuDtoTwo.setMenuIcon(sysMenuTwoDtoList.get(k)
                        .getMenuIcon());
                sysMenuDtoTwo.setMenuUrl(sysMenuTwoDtoList.get(k).getMenuUrl());
                sysMenuDtoTwo.setMenuPermission(sysMenuTwoDtoList.get(k)
                        .getMenuPermission());
                // 获取二级菜单下的三级菜单
                List<MenuInfoDto> sysMenuThreeDtoList = findSysMenuDtoThree(
                        menuInfoDtos, sysMenuTwoDtoList, k);
                sysMenuDtoTwo.setMenuInfoDtoList(sysMenuThreeDtoList);
                sysMenuTwoDtoListAdd.add(sysMenuDtoTwo);
            }
            sysMenuDtoOne.setMenuInfoDtoList(sysMenuTwoDtoListAdd);
            sysMenuAllDtoList.add(sysMenuDtoOne);
        }
        return sysMenuAllDtoList;

    }

    /**
     * 获取一级菜单.
     */
    public List<MenuInfoDto> findSysMenuDtoOne(List<MenuInfoDto> sysMenuDtoList) {
        List<MenuInfoDto> sysMenuOneDtoList = new ArrayList<MenuInfoDto>();
        for (int i = 0; i < sysMenuDtoList.size(); i++) {
            if ("1".equals(sysMenuDtoList.get(i).getMenuLevel())) {
                sysMenuOneDtoList.add(sysMenuDtoList.get(i));
            }
        }
        return sysMenuOneDtoList;
    }

    /**
     * 获取二级菜单.
     */
    public List<MenuInfoDto> findSysMenuDtoTwo(List<MenuInfoDto> sysMenuDtoList,
                                              List<MenuInfoDto> sysMenuOneDtoList, int i) {
        List<MenuInfoDto> sysMenuTwoDtoList = new ArrayList<MenuInfoDto>();
        for (int j = 0; j < sysMenuDtoList.size(); j++) {
            if ("2".equals(sysMenuDtoList.get(j).getMenuLevel())
                    && sysMenuDtoList.get(j).getParentMenu()
                    .equals(sysMenuOneDtoList.get(i).getMenuId())) {
                sysMenuTwoDtoList.add(sysMenuDtoList.get(j));
            }
        }
        return sysMenuTwoDtoList;
    }

    /**
     * 获取三级菜单.
     */
    public List<MenuInfoDto> findSysMenuDtoThree(
            List<MenuInfoDto> sysMenuDtoList,
            List<MenuInfoDto> sysMenuTwoDtoList, int k) {
        List<MenuInfoDto> sysMenuThreeDtoList = new ArrayList<MenuInfoDto>();
        for (int n = 0; n < sysMenuDtoList.size(); n++) {
            if ("3".equals(sysMenuDtoList.get(n).getMenuLevel())
                    && sysMenuDtoList.get(n).getParentMenu()
                    .equals(sysMenuTwoDtoList.get(k).getMenuId())) {
                sysMenuThreeDtoList.add(sysMenuDtoList.get(n));
            }
        }
        return sysMenuThreeDtoList;
    }
}
