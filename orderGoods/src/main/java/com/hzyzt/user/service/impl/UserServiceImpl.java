package com.hzyzt.user.service.impl;

import com.common.util.DateUtil;
import com.common.util.EnumHelper;
import com.common.util.StringUtil;
import com.hzyzt.login.dao.UserRoleDtoMapper;
import com.hzyzt.login.dto.UserRoleDtoKey;
import com.hzyzt.seq.service.SeqService;
import com.hzyzt.user.dao.UserDtoDao;
import com.hzyzt.user.dto.UserDto;
import com.hzyzt.user.enums.UserRoleTypeEnum;
import com.hzyzt.user.enums.UserTypeEnum;
import com.hzyzt.user.service.UserService;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Description :
 * @Author : 吴书新
 */
@Service
public class UserServiceImpl implements UserService{

    //默认密码
    private static final String DEFAUL_PASSWORD = "123456";

    @Autowired
    private UserDtoDao userDtoDao;

    @Autowired
    private SeqService seqService;

//    @Autowired
//    private SiteInfoService siteInfoService;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private UserRoleDtoMapper userRoleDtoMapper;

    @Override
    public UserDto getUserByUserName(String userLoginName) throws Exception{
        UserDto userDto = new UserDto();
        if(StringUtil.isStrNullOrEmpty(userLoginName)){
            return null;
        }
        userDto.setUserLoginName(userLoginName);
        List<UserDto> userDtos = userDtoDao.getUserByUserInfo(userDto);
        if(StringUtil.isObjNotNull(userDtos) && userDtos.size()>0){
            return userDtos.get(0);
        }else {
            return null;
        }
    }

    @Override
    public List<UserDto> getUserAll(UserDto userDto) throws Exception {
        return userDtoDao.getUserByUserInfo(userDto);
    }

    /**
     * @Description : 查询所有用户
     */
    @Override
    public List<UserDto> getUserAll(UserDto userDto, String userType) throws Exception {
        if(StringUtil.isObjNull(userDto)){
            userDto = new UserDto();
        }
        if(UserTypeEnum.ALL.getCode().equals(userType)){
            userDto.setUserType("");
        }else{
            userDto.setUserType(userType);
        }
        return this.getUserAll(userDto);
    }

    @Override
    public UserRoleDtoKey getUserRoles(UserDto user) {
        return null;
    }

    @Override
    public void addUserInfo(UserDto userDto) throws Exception {
        Integer userId = seqService.getUserId();
        if("B".equals(userDto.getUserType())){
            updateSiteInfo(userDto, userId);//为站点设置用户
        }
        if(StringUtil.isObjNotNull(userDto)){
            userDto.setUserId(userId);
        }else{
            throw new Exception("userDto为空");
        }
        userDto.setCreateTime(DateUtil.getDateString19(new Date()));
        if(StringUtil.isStrNotNullAndNotEmpty(userDto.getUserName())){
            // 密码转换
            AuthenticationToken token = new UsernamePasswordToken(userDto.getUserName(), DEFAUL_PASSWORD);
            userDto.setUserPassword(passwordService.encryptPassword(token.getCredentials()));
        }
        userDtoDao.addUserInfo(userDto);
        //角色
        UserRoleDtoKey userRoleDtoKey = new UserRoleDtoKey(EnumHelper.translate(UserRoleTypeEnum.class,userDto.getUserType()).getText(),userId);
        userRoleDtoMapper.insert(userRoleDtoKey);
    }

    /**
     * @Description :设置用户
     */
    private void updateSiteInfo(UserDto userDto, Integer userId) throws Exception {
        if(StringUtil.isObjNull(userDto) || StringUtil.isObjNull(userDto.getSiteId()) || userDto.getSiteId() == 0){
            throw new Exception("siteId不能为空");
        }
//        SiteInfoDto siteInfoDto = new SiteInfoDto();
//        siteInfoDto.setSiteId(userDto.getSiteId());
//        siteInfoDto.setUserId(userId);
//        siteInfoService.updateSiteInfo(siteInfoDto);
    }

    @Override
    public void updateUserInfo(UserDto userDto) throws Exception{
        if(StringUtil.isStrNotNullAndNotEmpty(userDto.getUserPassword())){
            // 密码转换
            AuthenticationToken token = new UsernamePasswordToken(userDto.getUserName(), userDto.getUserPassword());
            userDto.setUserPassword(passwordService.encryptPassword(token.getCredentials()));
        }
        userDtoDao.updateUserInfo(userDto);
    }

    @Override
    public UserDto getUserByUserId(String userId) throws Exception{
        if(StringUtil.isStrNullOrEmpty(userId)){
            return null;
        }
        UserDto userDto = new UserDto();
        userDto.setUserId(Integer.parseInt(userId));
        List<UserDto> userDtoList = this.getUserAll(userDto);
        if(StringUtil.isListNotNull(userDtoList)){
            return userDtoList.get(0);
        }
        return null;
    }
}
