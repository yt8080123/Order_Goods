package com.hzyzt.user.service;

import com.hzyzt.user.dto.UserDto;
import com.hzyzt.login.dto.UserRoleDtoKey;

import java.util.List;

/**
 * @Description :
 * @Author : 吴书新
 */
public interface UserService {

    /**
     * @Description : 通过用户名，查询用户信息
     */
    UserDto getUserByUserName(String userName) throws Exception;

    List<UserDto> getUserAll(UserDto userDto) throws Exception;

    /**
     * @Description : 查询所有用户
     */
    List<UserDto> getUserAll(UserDto userDto, String userType) throws Exception;

    UserRoleDtoKey getUserRoles(UserDto user);

    void addUserInfo(UserDto userDto) throws Exception;

    void updateUserInfo(UserDto userDto) throws Exception;

    UserDto getUserByUserId(String userId) throws Exception;
}
