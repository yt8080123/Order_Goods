package com.hzyzt.user.dao;

import java.util.List;

/**
 * @Description :
 * @Author : 吴书新
 * @Date : 2017/2/24 0024 21:38
 */
public interface UserPermissionMapper {

    List<String> getUserPermission(String userId);

}
