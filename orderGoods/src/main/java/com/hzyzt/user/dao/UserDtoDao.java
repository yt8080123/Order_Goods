package com.hzyzt.user.dao;

import com.hzyzt.user.dto.UserDto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDtoDao {
    List<UserDto> getUserByUserInfo(UserDto userDto);

    void addUserInfo(UserDto userDto);

    void updateUserInfo(UserDto userDto);
}