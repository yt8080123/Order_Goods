package com.hzyzt.user.dao;

import com.hzyzt.user.dto.MenuInfoDto;

import java.util.List;

public interface MenuInfoMapper {

    int insert(MenuInfoDto record);

    MenuInfoDto selectByPrimaryKey(String menuId);

    int updateByPrimaryKeySelective(MenuInfoDto record);

    List<MenuInfoDto> getAllMenu(List<String> prems);
}