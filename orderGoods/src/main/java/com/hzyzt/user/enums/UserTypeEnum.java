package com.hzyzt.user.enums;

/**
 * @Description :
 * @Author : 吴书新
 */
public enum UserTypeEnum {
    A("A","管理员"),
    B("B","站点用户"),
    ALL("ALL","全体用户");

    private UserTypeEnum(String code,String text){
        this.code = code;
        this.text = text;
    }

    private String code;
    private String text;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
