package com.hzyzt.user.enums;

/**
 * @Description :
 * @Author : 吴书新
 */
public enum UserRoleTypeEnum {
    A("A","admin"),
    B("B","user");

    private String code;
    private String text;

    UserRoleTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
