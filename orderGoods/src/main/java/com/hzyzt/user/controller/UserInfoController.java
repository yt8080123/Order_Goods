package com.hzyzt.user.controller;

import com.common.constant.ApplicationConstant;
import com.common.dto.PageObjectDto;
import com.common.dto.ResultDto;
import com.common.dto.ResultDtoFactory;
import com.common.util.*;
import com.github.pagehelper.PageHelper;
import com.hzyzt.user.dto.MenuInfoDto;
import com.hzyzt.user.dto.UserDto;
import com.hzyzt.user.enums.UserTypeEnum;
import com.hzyzt.user.service.MenuInfoService;
import com.hzyzt.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description :
 */
@RequestMapping(value = "userInfo")
@Controller
public class UserInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MenuInfoService menuInfoService;

    private static final String MODEL_MENU = "menu";

    private static final String MODEL_USERNAME = "userName";

    @RequestMapping("openUserPage")
    public String openUserPage(Model model,HttpSession httpSession){

        try {
            List<MenuInfoDto> menuInfoDtos = menuInfoService.getAllMenu();
            model.addAttribute(MODEL_MENU,menuInfoDtos);
            UserDto userDto = (UserDto) httpSession.getAttribute(ApplicationConstant.SESSIONUSER);
            model.addAttribute(MODEL_USERNAME,userDto.getUserName());
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute(MODEL_MENU,null);
        }
        return "user/user";
    }


    @RequestMapping(value = "getUserInfoList")
    @ResponseBody
    public PageObjectDto<UserDto> getUserInfoList(@RequestBody UserDto userDto){
        PageHelper.startPage(userDto.getPage(), userDto.getRows());
        try {
            //System.out.println(WebUtil.getSessionUser().getUserLoginName());
            List<UserDto> userDtoList = userService.getUserAll(userDto);
            for(UserDto userDto1 : userDtoList){
                userDto1.setUserType(EnumHelper.translate(UserTypeEnum.class,userDto1.getUserType()).getText());
            }
            return PageUtil.getPageInfo(userDtoList,userDto);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "openAddUserPage")
    public String openAddUserPage(Model model){
        /*List<SiteInfoDto> siteInfoDtos = new ArrayList<SiteInfoDto>();
        try {
            siteInfoDtos = siteInfoService.getSiteList(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("siteList",siteInfoDtos);*/
        return "user/addUserPage";
    }

    /**
     * @Description : 后台管理员新增用户信息.
     */
    @RequestMapping(value = "addUserInfo")
    @ResponseBody
    public ResultDto addUserInfo(@RequestBody UserDto userDto){
        try {
            userService.addUserInfo(userDto);
            return ResultDtoFactory.toAck("新增用户成功！");
        }catch (Exception e){
            StackTraceUtil.getStackTrace(e);
            return ResultDtoFactory.toNack("新增用户失败！");
        }
    }

    /**
     * @Description : 修改用户信息.
     */
    @RequestMapping(value = "updateUserInfo")
    @ResponseBody
    public ResultDto updateUserInfo(@RequestBody UserDto userDto){
        try {
            userService.updateUserInfo(userDto);
            return ResultDtoFactory.toAck("修改用户信息成功！");
        }catch (Exception e){
            StackTraceUtil.getStackTrace(e);
            return ResultDtoFactory.toNack("修改用户信息失败！");
        }
    }

    @RequestMapping(value = "openEditUser")
    public String openEditUser(Model model, @RequestParam String userId){
        boolean ifMySelf = false;
        try {
            if(StringUtil.isStrNullOrEmpty(userId)){
                userId = WebUtil.getSessionUserId();
                ifMySelf = true;
            }
            UserDto userDto = userService.getUserByUserId(userId);
            model.addAttribute("userDto",userDto);
        } catch (Exception e) {
            LOGGER.error(StackTraceUtil.getStackTrace(e));
            model.addAttribute("userDto",null);
        }
        model.addAttribute("ifMySelf",ifMySelf);
        return "user/editUser";
    }

    @RequestMapping(value = "editUserInfo")
    @ResponseBody
    public ResultDto editUserInfo(@RequestBody UserDto userDto){
        try {
            userService.updateUserInfo(userDto);
        }catch (Exception e){
            LOGGER.error(StackTraceUtil.getStackTrace(e));
            return ResultDtoFactory.toNack("失败");
        }
        return ResultDtoFactory.toAck("成功");
    }

}
