package com.hzyzt.seq.service.impl;

import com.hzyzt.seq.service.SeqService;
import com.common.util.SeqUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * id自动增加实现类.
 * @author wafawang
 *
 */
@Transactional
@Service
public class SeqServiceImpl extends SeqUtil implements SeqService {


	@Override
	public Integer getUserId() {
		return this.getAndIncreaseSeq("table_user", "*","*");
	}

}
