package com.hzyzt.seq.service;
/**
 * 
 * @author 吴书新
 *
 */
public interface SeqService {

	/**
	 * @Author : 吴书新
	 * @Description : 得到user表主键序列值
	 * @return : 主键序列值
	 * @throw :
	 */
	Integer getUserId();

}
