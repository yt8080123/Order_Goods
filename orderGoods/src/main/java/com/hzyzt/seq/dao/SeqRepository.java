package com.hzyzt.seq.dao;

import java.util.Map;
/**
 * 
 * @author fanghu
 *
 */
public interface SeqRepository {

	int insertSeq(Map<String, Object> map);

	int updateSeq(Map<String, Object> map);	

	int selectOne(Map<String, Object> map);	

	int selectLockStatus(Map<String, Object> map);

	int updateLockStatus(Map<String, Object> map);
}
