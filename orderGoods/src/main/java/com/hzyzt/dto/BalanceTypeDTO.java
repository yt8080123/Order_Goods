package com.hzyzt.dto;

import lombok.Data;

/**
 * 收支类型表(BALANCE_TYPE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class BalanceTypeDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 7140252587513400898L;

    /**  */
    private Integer id;

    /**
     * 收支类型
     */
    private String bType;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * 描述
     */
    private String describe;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取收支类型
     *
     * @return 收支类型
     */
    public String getBType() {
        return this.bType;
    }

    /**
     * 设置收支类型
     *
     * @param bType 收支类型
     */
    public void setBType(String bType) {
        this.bType = bType;
    }

    /**
     * 获取类型名称
     *
     * @return 类型名称
     */
    public String getTypeName() {
        return this.typeName;
    }

    /**
     * 设置类型名称
     *
     * @param typeName 类型名称
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * 获取描述
     *
     * @return 描述
     */
    public String getDescribe() {
        return this.describe;
    }

    /**
     * 设置描述
     *
     * @param describe 描述
     */
    public void setDescribe(String describe) {
        this.describe = describe;
    }
}