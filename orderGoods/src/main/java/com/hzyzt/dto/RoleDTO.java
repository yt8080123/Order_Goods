package com.hzyzt.dto;

import lombok.Data;

/**
 * 角色管理表(ROLE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class RoleDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -2507413648467413739L;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色编号
     */
    private String roleCode;

    /**
     * 获取角色名
     *
     * @return 角色名
     */
    public String getRoleName() {
        return this.roleName;
    }

    /**
     * 设置角色名
     *
     * @param roleName 角色名
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取角色编号
     *
     * @return 角色编号
     */
    public String getRoleCode() {
        return this.roleCode;
    }

    /**
     * 设置角色编号
     *
     * @param roleCode 角色编号
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}