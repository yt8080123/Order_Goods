package com.hzyzt.dto;

import lombok.Data;

/**
 * 订货单表(INDENT)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class IndentDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 4099869773353016088L;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 下单时间
     */
    private String orderTime;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 订单金额
     */
    private String orderAmount;

    /**
     * 订单状态(1:待核价,2:待审核,3:待出库,4:待发货,5待收货,6:已收货,7:已取消,0:强制完成)
     */
    private String oStatus;

    /**
     * 收款状态（1:待收款,2:待确认,3:部分确认,4:部分收款,0:已收款）
     */
    private String makeStatus;

    /**
     * 获取订单编号
     *
     * @return 订单编号
     */
    public String getOrderId() {
        return this.orderId;
    }

    /**
     * 设置订单编号
     *
     * @param orderId 订单编号
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取下单时间
     *
     * @return 下单时间
     */
    public String getOrderTime() {
        return this.orderTime;
    }

    /**
     * 设置下单时间
     *
     * @param orderTime 下单时间
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取联系人
     *
     * @return 联系人
     */
    public String getContacts() {
        return this.contacts;
    }

    /**
     * 设置联系人
     *
     * @param contacts 联系人
     */
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    /**
     * 获取订单金额
     *
     * @return 订单金额
     */
    public String getOrderAmount() {
        return this.orderAmount;
    }

    /**
     * 设置订单金额
     *
     * @param orderAmount 订单金额
     */
    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * 获取订单状态(1:待核价,2:待审核,3:待出库,4:待发货,5待收货,6:已收货,7:已取消,0:强制完成)
     *
     * @return 订单状态(1 : 待核价
     */
    public String getOStatus() {
        return this.oStatus;
    }

    /**
     * 设置订单状态(1:待核价,2:待审核,3:待出库,4:待发货,5待收货,6:已收货,7:已取消,0:强制完成)
     *
     * @param oStatus 订单状态(1:待核价,2:待审核,3:待出库,4:待发货,5待收货,6:已收货,7:已取消,0:强制完成)
     */
    public void setOStatus(String oStatus) {
        this.oStatus = oStatus;
    }

    /**
     * 获取收款状态（1:待收款,2:待确认,3:部分确认,4:部分收款,0:已收款）
     *
     * @return 收款状态（1:待收款
     */
    public String getMakeStatus() {
        return this.makeStatus;
    }

    /**
     * 设置收款状态（1:待收款,2:待确认,3:部分确认,4:部分收款,0:已收款）
     *
     * @param makeStatus 收款状态（1:待收款,2:待确认,3:部分确认,4:部分收款,0:已收款）
     */
    public void setMakeStatus(String makeStatus) {
        this.makeStatus = makeStatus;
    }
}