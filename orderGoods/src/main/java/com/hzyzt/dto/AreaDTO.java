package com.hzyzt.dto;

import lombok.Data;

/**
 * 客户归属地区表(AREA)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class AreaDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -686057628323965535L;

    /**  */
    private Integer id;

    /**
     * 地区编号
     */
    private String regionNumber;

    /**
     * 地区名字
     */
    private String regionalName;

    /**
     * 上级地区
     */
    private String superiorRegion;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取地区编号
     *
     * @return 地区编号
     */
    public String getRegionNumber() {
        return this.regionNumber;
    }

    /**
     * 设置地区编号
     *
     * @param regionNumber 地区编号
     */
    public void setRegionNumber(String regionNumber) {
        this.regionNumber = regionNumber;
    }

    /**
     * 获取地区名字
     *
     * @return 地区名字
     */
    public String getRegionalName() {
        return this.regionalName;
    }

    /**
     * 设置地区名字
     *
     * @param regionalName 地区名字
     */
    public void setRegionalName(String regionalName) {
        this.regionalName = regionalName;
    }

    /**
     * 获取上级地区
     *
     * @return 上级地区
     */
    public String getSuperiorRegion() {
        return this.superiorRegion;
    }

    /**
     * 设置上级地区
     *
     * @param superiorRegion 上级地区
     */
    public void setSuperiorRegion(String superiorRegion) {
        this.superiorRegion = superiorRegion;
    }
}