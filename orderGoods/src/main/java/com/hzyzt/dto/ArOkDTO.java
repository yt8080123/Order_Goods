package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 收款确认表(AR_OK)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class ArOkDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 4204317463195616048L;

    /**
     * 编号
     */
    private Integer id;

    /**
     * 关联单号
     */
    private String arId;

    /**
     * 支付时间
     */
    private String payTime;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     */
    private String payMode;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 收款银行
     */
    private String arBank;

    /**
     * 收款状态(1:已确认,2:待确认,0:已取消)
     */
    private String arStatus;

    /**
     * 获取编号
     *
     * @return 编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取关联单号
     *
     * @return 关联单号
     */
    public String getArId() {
        return this.arId;
    }

    /**
     * 设置关联单号
     *
     * @param arId 关联单号
     */
    public void setArId(String arId) {
        this.arId = arId;
    }

    /**
     * 获取支付时间
     *
     * @return 支付时间
     */
    public String getPayTime() {
        return this.payTime;
    }

    /**
     * 设置支付时间
     *
     * @param payTime 支付时间
     */
    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     *
     * @return 支付方式(1 : 支付宝支付
     */
    public String getPayMode() {
        return this.payMode;
    }

    /**
     * 设置支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     *
     * @param payMode 支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取收款银行
     *
     * @return 收款银行
     */
    public String getArBank() {
        return this.arBank;
    }

    /**
     * 设置收款银行
     *
     * @param arBank 收款银行
     */
    public void setArBank(String arBank) {
        this.arBank = arBank;
    }

    /**
     * 获取收款状态(1:已确认,2:待确认,0:已取消)
     *
     * @return 收款状态(1 : 已确认
     */
    public String getArStatus() {
        return this.arStatus;
    }

    /**
     * 设置收款状态(1:已确认,2:待确认,0:已取消)
     *
     * @param arStatus 收款状态(1:已确认,2:待确认,0:已取消)
     */
    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }
}