package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品表(GOODS)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class GoodsDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -5948856522030460201L;

    /**  */
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品分类
     */
    private String cId;

    /**
     * 所属仓库
     */
    private String rId;

    /**
     * 上架下架(1:上架，0:下架)
     */
    private String upperFrame;

    /**
     * 排序值
     */
    private String sort;

    /**
     * 商品品牌
     */
    private String bId;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 库位号
     */
    private String number;

    /**
     * 供应商
     */
    private String sId;

    /**
     * 商品型号
     */
    private String model;

    /**
     * 商品标签(1:新品,2:推荐,3:热销,4:赠品)
     */
    private String tag;

    /**
     * 单位
     */
    private String uId;

    /**
     * 价格
     */
    private String pId;

    /**
     * 库存
     */
    private String inventory;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品名称
     *
     * @return 商品名称
     */
    public String getGoodsName() {
        return this.goodsName;
    }

    /**
     * 设置商品名称
     *
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取商品分类
     *
     * @return 商品分类
     */
    public String getCId() {
        return this.cId;
    }

    /**
     * 设置商品分类
     *
     * @param cId 商品分类
     */
    public void setCId(String cId) {
        this.cId = cId;
    }

    /**
     * 获取所属仓库
     *
     * @return 所属仓库
     */
    public String getRId() {
        return this.rId;
    }

    /**
     * 设置所属仓库
     *
     * @param rId 所属仓库
     */
    public void setRId(String rId) {
        this.rId = rId;
    }

    /**
     * 获取上架下架(1:上架，0:下架)
     *
     * @return 上架下架(1 : 上架
     */
    public String getUpperFrame() {
        return this.upperFrame;
    }

    /**
     * 设置上架下架(1:上架，0:下架)
     *
     * @param upperFrame 上架下架(1:上架，0:下架)
     */
    public void setUpperFrame(String upperFrame) {
        this.upperFrame = upperFrame;
    }

    /**
     * 获取排序值
     *
     * @return 排序值
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置排序值
     *
     * @param sort 排序值
     */
    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * 获取商品品牌
     *
     * @return 商品品牌
     */
    public String getBId() {
        return this.bId;
    }

    /**
     * 设置商品品牌
     *
     * @param bId 商品品牌
     */
    public void setBId(String bId) {
        this.bId = bId;
    }

    /**
     * 获取关键字
     *
     * @return 关键字
     */
    public String getKeyword() {
        return this.keyword;
    }

    /**
     * 设置关键字
     *
     * @param keyword 关键字
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * 获取库位号
     *
     * @return 库位号
     */
    public String getNumber() {
        return this.number;
    }

    /**
     * 设置库位号
     *
     * @param number 库位号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSId() {
        return this.sId;
    }

    /**
     * 设置供应商
     *
     * @param sId 供应商
     */
    public void setSId(String sId) {
        this.sId = sId;
    }

    /**
     * 获取商品型号
     *
     * @return 商品型号
     */
    public String getModel() {
        return this.model;
    }

    /**
     * 设置商品型号
     *
     * @param model 商品型号
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 获取商品标签(1:新品,2:推荐,3:热销,4:赠品)
     *
     * @return 商品标签(1 : 新品
     */
    public String getTag() {
        return this.tag;
    }

    /**
     * 设置商品标签(1:新品,2:推荐,3:热销,4:赠品)
     *
     * @param tag 商品标签(1:新品,2:推荐,3:热销,4:赠品)
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 获取单位
     *
     * @return 单位
     */
    public String getUId() {
        return this.uId;
    }

    /**
     * 设置单位
     *
     * @param uId 单位
     */
    public void setUId(String uId) {
        this.uId = uId;
    }

    /**
     * 获取价格
     *
     * @return 价格
     */
    public String getPId() {
        return this.pId;
    }

    /**
     * 设置价格
     *
     * @param pId 价格
     */
    public void setPId(String pId) {
        this.pId = pId;
    }

    /**
     * 获取库存
     *
     * @return 库存
     */
    public String getInventory() {
        return this.inventory;
    }

    /**
     * 设置库存
     *
     * @param inventory 库存
     */
    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
}