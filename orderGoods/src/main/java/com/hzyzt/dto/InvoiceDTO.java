package com.hzyzt.dto;

import lombok.Data;

/**
 * 发货单表(INVOICE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class InvoiceDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -4477862347720544863L;

    /**  */
    private Integer id;

    /**
     * 出口单号
     */
    private String outgoing;

    /**
     * 出库时间
     */
    private String outgoingTime;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 关联订单
     */
    private String oId;

    /**
     * 发货仓库
     */
    private String sendId;

    /**
     * 发货状态(1:带发货，2:待收货,3:已收货,4:已取消)
     */
    private String sendStatus;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取出口单号
     *
     * @return 出口单号
     */
    public String getOutgoing() {
        return this.outgoing;
    }

    /**
     * 设置出口单号
     *
     * @param outgoing 出口单号
     */
    public void setOutgoing(String outgoing) {
        this.outgoing = outgoing;
    }

    /**
     * 获取出库时间
     *
     * @return 出库时间
     */
    public String getOutgoingTime() {
        return this.outgoingTime;
    }

    /**
     * 设置出库时间
     *
     * @param outgoingTime 出库时间
     */
    public void setOutgoingTime(String outgoingTime) {
        this.outgoingTime = outgoingTime;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取关联订单
     *
     * @return 关联订单
     */
    public String getOId() {
        return this.oId;
    }

    /**
     * 设置关联订单
     *
     * @param oId 关联订单
     */
    public void setOId(String oId) {
        this.oId = oId;
    }

    /**
     * 获取发货仓库
     *
     * @return 发货仓库
     */
    public String getSendId() {
        return this.sendId;
    }

    /**
     * 设置发货仓库
     *
     * @param sendId 发货仓库
     */
    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    /**
     * 获取发货状态(1:带发货，2:待收货,3:已收货,4:已取消)
     *
     * @return 发货状态(1 : 带发货
     */
    public String getSendStatus() {
        return this.sendStatus;
    }

    /**
     * 设置发货状态(1:带发货，2:待收货,3:已收货,4:已取消)
     *
     * @param sendStatus 发货状态(1:带发货，2:待收货,3:已收货,4:已取消)
     */
    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }
}