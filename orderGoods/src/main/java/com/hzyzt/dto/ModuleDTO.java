package com.hzyzt.dto;

import lombok.Data;

/**
 * 盘点单表(MODULE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class ModuleDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -4404405551600468992L;

    /**  */
    private Integer id;

    /**
     * 盘点单号
     */
    private String mId;

    /**
     * 盘点名称
     */
    private String mName;

    /**
     * 盘点仓库
     */
    private String mWarehouse;

    /**
     * 经办人
     */
    private String agent;

    /**
     * 审核状态(1:待审核,2:已审核,0:已取消)
     */
    private String mStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取盘点单号
     *
     * @return 盘点单号
     */
    public String getMId() {
        return this.mId;
    }

    /**
     * 设置盘点单号
     *
     * @param mId 盘点单号
     */
    public void setMId(String mId) {
        this.mId = mId;
    }

    /**
     * 获取盘点名称
     *
     * @return 盘点名称
     */
    public String getMName() {
        return this.mName;
    }

    /**
     * 设置盘点名称
     *
     * @param mName 盘点名称
     */
    public void setMName(String mName) {
        this.mName = mName;
    }

    /**
     * 获取盘点仓库
     *
     * @return 盘点仓库
     */
    public String getMWarehouse() {
        return this.mWarehouse;
    }

    /**
     * 设置盘点仓库
     *
     * @param mWarehouse 盘点仓库
     */
    public void setMWarehouse(String mWarehouse) {
        this.mWarehouse = mWarehouse;
    }

    /**
     * 获取经办人
     *
     * @return 经办人
     */
    public String getAgent() {
        return this.agent;
    }

    /**
     * 设置经办人
     *
     * @param agent 经办人
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * 获取审核状态(1:待审核,2:已审核,0:已取消)
     *
     * @return 审核状态(1 : 待审核
     */
    public String getMStatus() {
        return this.mStatus;
    }

    /**
     * 设置审核状态(1:待审核,2:已审核,0:已取消)
     *
     * @param mStatus 审核状态(1:待审核,2:已审核,0:已取消)
     */
    public void setMStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}