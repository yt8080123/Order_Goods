package com.hzyzt.dto;

import lombok.Data;

/**
 * 库存状况表(STOCK)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class StockDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -1630511844048953486L;

    /**  */
    private Integer id;

    /**
     * 商品
     */
    private String goods;

    /**
     * 单位
     */
    private String unit;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 可售库存
     */
    private String avaStock;

    /**
     * 实际库存
     */
    private String actualStock;

    /**
     * 安全库存
     */
    private String safeStock;

    /**
     * 库存上限
     */
    private String stockUpper;

    /**
     * 库存下限
     */
    private String stockLower;

    /**
     * 可售/实际库存状态(1:高于上限,2:低于下限,3:低于安全库存,0:缺货 )
     */
    private String stockStatus;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品
     *
     * @return 商品
     */
    public String getGoods() {
        return this.goods;
    }

    /**
     * 设置商品
     *
     * @param goods 商品
     */
    public void setGoods(String goods) {
        this.goods = goods;
    }

    /**
     * 获取单位
     *
     * @return 单位
     */
    public String getUnit() {
        return this.unit;
    }

    /**
     * 设置单位
     *
     * @param unit 单位
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 获取仓库
     *
     * @return 仓库
     */
    public String getWarehouse() {
        return this.warehouse;
    }

    /**
     * 设置仓库
     *
     * @param warehouse 仓库
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * 获取可售库存
     *
     * @return 可售库存
     */
    public String getAvaStock() {
        return this.avaStock;
    }

    /**
     * 设置可售库存
     *
     * @param avaStock 可售库存
     */
    public void setAvaStock(String avaStock) {
        this.avaStock = avaStock;
    }

    /**
     * 获取实际库存
     *
     * @return 实际库存
     */
    public String getActualStock() {
        return this.actualStock;
    }

    /**
     * 设置实际库存
     *
     * @param actualStock 实际库存
     */
    public void setActualStock(String actualStock) {
        this.actualStock = actualStock;
    }

    /**
     * 获取安全库存
     *
     * @return 安全库存
     */
    public String getSafeStock() {
        return this.safeStock;
    }

    /**
     * 设置安全库存
     *
     * @param safeStock 安全库存
     */
    public void setSafeStock(String safeStock) {
        this.safeStock = safeStock;
    }

    /**
     * 获取库存上限
     *
     * @return 库存上限
     */
    public String getStockUpper() {
        return this.stockUpper;
    }

    /**
     * 设置库存上限
     *
     * @param stockUpper 库存上限
     */
    public void setStockUpper(String stockUpper) {
        this.stockUpper = stockUpper;
    }

    /**
     * 获取库存下限
     *
     * @return 库存下限
     */
    public String getStockLower() {
        return this.stockLower;
    }

    /**
     * 设置库存下限
     *
     * @param stockLower 库存下限
     */
    public void setStockLower(String stockLower) {
        this.stockLower = stockLower;
    }

    /**
     * 获取可售/实际库存状态(1:高于上限,2:低于下限,3:低于安全库存,0:缺货 )
     *
     * @return 可售/实际库存状态(1:高于上限
     */
    public String getStockStatus() {
        return this.stockStatus;
    }

    /**
     * 设置可售/实际库存状态(1:高于上限,2:低于下限,3:低于安全库存,0:缺货 )
     *
     * @param stockStatus 可售/实际库存状态(1:高于上限,2:低于下限,3:低于安全库存,0:缺货 )
     */
    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }
}