package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品图片表(PICTURE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class PictureDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 5143892513279132725L;

    /**  */
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 图片
     */
    private String pic;

    /**  */
    private String sort;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品名称
     *
     * @return 商品名称
     */
    public String getGoodsName() {
        return this.goodsName;
    }

    /**
     * 设置商品名称
     *
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取图片
     *
     * @return 图片
     */
    public String getPic() {
        return this.pic;
    }

    /**
     * 设置图片
     *
     * @param pic 图片
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 获取
     *
     * @return
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置
     *
     * @param sort
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
}