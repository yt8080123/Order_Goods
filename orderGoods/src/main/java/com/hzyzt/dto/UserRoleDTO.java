package com.hzyzt.dto;

import lombok.Data;

/**
 * 用户角色(USER_ROLE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class UserRoleDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -1317630552457624775L;

    /**
     * 用户主键
     */
    private Integer userId;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 获取用户主键
     *
     * @return 用户主键
     */
    public Integer getUserId() {
        return this.userId;
    }

    /**
     * 设置用户主键
     *
     * @param userId 用户主键
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取角色名
     *
     * @return 角色名
     */
    public String getRoleName() {
        return this.roleName;
    }

    /**
     * 设置角色名
     *
     * @param roleName 角色名
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}