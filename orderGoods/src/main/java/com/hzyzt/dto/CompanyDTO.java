package com.hzyzt.dto;

import lombok.Data;

/**
 * 公司资料表(COMPANY)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class CompanyDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -4216917421310316475L;

    /**  */
    private Integer id;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 属于行业
     */
    private String belongIndustry;

    /**
     * 公司地址
     */
    private String companyAddress;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 传真
     */
    private String fax;

    /**
     * 联系人id
     */
    private String contactsId;

    /**
     * 联系电话
     */
    private String mobileNumber;

    /**
     * 公司介绍
     */
    private String companyIntroduction;

    /**
     * 开户名称
     */
    private String accountName;

    /**
     * 开户银行
     */
    private String accountBank;

    /**
     * 银行账号
     */
    private String bankNumber;

    /**
     * 开票抬头
     */
    private String rise;

    /**
     * 纳税人识别号
     */
    private String identifyNumber;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取公司名称
     *
     * @return 公司名称
     */
    public String getCompanyName() {
        return this.companyName;
    }

    /**
     * 设置公司名称
     *
     * @param companyName 公司名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 获取属于行业
     *
     * @return 属于行业
     */
    public String getBelongIndustry() {
        return this.belongIndustry;
    }

    /**
     * 设置属于行业
     *
     * @param belongIndustry 属于行业
     */
    public void setBelongIndustry(String belongIndustry) {
        this.belongIndustry = belongIndustry;
    }

    /**
     * 获取公司地址
     *
     * @return 公司地址
     */
    public String getCompanyAddress() {
        return this.companyAddress;
    }

    /**
     * 设置公司地址
     *
     * @param companyAddress 公司地址
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * 获取邮箱
     *
     * @return 邮箱
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取传真
     *
     * @return 传真
     */
    public String getFax() {
        return this.fax;
    }

    /**
     * 设置传真
     *
     * @param fax 传真
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * 获取联系人id
     *
     * @return 联系人id
     */
    public String getContactsId() {
        return this.contactsId;
    }

    /**
     * 设置联系人id
     *
     * @param contactsId 联系人id
     */
    public void setContactsId(String contactsId) {
        this.contactsId = contactsId;
    }

    /**
     * 获取联系电话
     *
     * @return 联系电话
     */
    public String getMobileNumber() {
        return this.mobileNumber;
    }

    /**
     * 设置联系电话
     *
     * @param mobileNumber 联系电话
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * 获取公司介绍
     *
     * @return 公司介绍
     */
    public String getCompanyIntroduction() {
        return this.companyIntroduction;
    }

    /**
     * 设置公司介绍
     *
     * @param companyIntroduction 公司介绍
     */
    public void setCompanyIntroduction(String companyIntroduction) {
        this.companyIntroduction = companyIntroduction;
    }

    /**
     * 获取开户名称
     *
     * @return 开户名称
     */
    public String getAccountName() {
        return this.accountName;
    }

    /**
     * 设置开户名称
     *
     * @param accountName 开户名称
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * 获取开户银行
     *
     * @return 开户银行
     */
    public String getAccountBank() {
        return this.accountBank;
    }

    /**
     * 设置开户银行
     *
     * @param accountBank 开户银行
     */
    public void setAccountBank(String accountBank) {
        this.accountBank = accountBank;
    }

    /**
     * 获取银行账号
     *
     * @return 银行账号
     */
    public String getBankNumber() {
        return this.bankNumber;
    }

    /**
     * 设置银行账号
     *
     * @param bankNumber 银行账号
     */
    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    /**
     * 获取开票抬头
     *
     * @return 开票抬头
     */
    public String getRise() {
        return this.rise;
    }

    /**
     * 设置开票抬头
     *
     * @param rise 开票抬头
     */
    public void setRise(String rise) {
        this.rise = rise;
    }

    /**
     * 获取纳税人识别号
     *
     * @return 纳税人识别号
     */
    public String getIdentifyNumber() {
        return this.identifyNumber;
    }

    /**
     * 设置纳税人识别号
     *
     * @param identifyNumber 纳税人识别号
     */
    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }
}