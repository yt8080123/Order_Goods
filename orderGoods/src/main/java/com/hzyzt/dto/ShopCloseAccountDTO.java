package com.hzyzt.dto;

import lombok.Data;

/**
 * 店铺结算表(SHOP_CLOSE_ACCOUNT)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class ShopCloseAccountDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -3575205743238585590L;

    /**  */
    private Integer id;

    /**
     * 店铺数量（统计店铺数量）
     */
    private Integer shopNumber;

    /**
     * 分润比例
     */
    private String fraction;

    /**
     * 销售金额
     */
    private String saleMoney;

    /**
     * 分润金额
     */
    private String moistureContent;

    /**
     * 结算（0未结算，1已结算）
     */
    private String settlement;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取店铺数量（统计店铺数量）
     *
     * @return 店铺数量（统计店铺数量）
     */
    public Integer getShopNumber() {
        return this.shopNumber;
    }

    /**
     * 设置店铺数量（统计店铺数量）
     *
     * @param shopNumber 店铺数量（统计店铺数量）
     */
    public void setShopNumber(Integer shopNumber) {
        this.shopNumber = shopNumber;
    }

    /**
     * 获取分润比例
     *
     * @return 分润比例
     */
    public String getFraction() {
        return this.fraction;
    }

    /**
     * 设置分润比例
     *
     * @param fraction 分润比例
     */
    public void setFraction(String fraction) {
        this.fraction = fraction;
    }

    /**
     * 获取销售金额
     *
     * @return 销售金额
     */
    public String getSaleMoney() {
        return this.saleMoney;
    }

    /**
     * 设置销售金额
     *
     * @param saleMoney 销售金额
     */
    public void setSaleMoney(String saleMoney) {
        this.saleMoney = saleMoney;
    }

    /**
     * 获取分润金额
     *
     * @return 分润金额
     */
    public String getMoistureContent() {
        return this.moistureContent;
    }

    /**
     * 设置分润金额
     *
     * @param moistureContent 分润金额
     */
    public void setMoistureContent(String moistureContent) {
        this.moistureContent = moistureContent;
    }

    /**
     * 获取结算（0未结算，1已结算）
     *
     * @return 结算（0未结算
     */
    public String getSettlement() {
        return this.settlement;
    }

    /**
     * 设置结算（0未结算，1已结算）
     *
     * @param settlement 结算（0未结算，1已结算）
     */
    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }
}