package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品回收站表(RECYCLE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class RecycleDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -4478937807949379578L;

    /**  */
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 单位
     */
    private String unitName;

    /**
     * 订货价
     */
    private String orderPrice;

    /**
     * 市场价
     */
    private String marketPrice;

    /**
     * 参考价
     */
    private String referencePrice;

    /**
     * 删除时间
     */
    private String deleteTime;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品名称
     *
     * @return 商品名称
     */
    public String getGoodsName() {
        return this.goodsName;
    }

    /**
     * 设置商品名称
     *
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取单位
     *
     * @return 单位
     */
    public String getUnitName() {
        return this.unitName;
    }

    /**
     * 设置单位
     *
     * @param unitName 单位
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * 获取订货价
     *
     * @return 订货价
     */
    public String getOrderPrice() {
        return this.orderPrice;
    }

    /**
     * 设置订货价
     *
     * @param orderPrice 订货价
     */
    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    /**
     * 获取市场价
     *
     * @return 市场价
     */
    public String getMarketPrice() {
        return this.marketPrice;
    }

    /**
     * 设置市场价
     *
     * @param marketPrice 市场价
     */
    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * 获取参考价
     *
     * @return 参考价
     */
    public String getReferencePrice() {
        return this.referencePrice;
    }

    /**
     * 设置参考价
     *
     * @param referencePrice 参考价
     */
    public void setReferencePrice(String referencePrice) {
        this.referencePrice = referencePrice;
    }

    /**
     * 获取删除时间
     *
     * @return 删除时间
     */
    public String getDeleteTime() {
        return this.deleteTime;
    }

    /**
     * 设置删除时间
     *
     * @param deleteTime 删除时间
     */
    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }
}