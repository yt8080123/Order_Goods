package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 采购商品明细表(GOODS_DETAILED)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class GoodsDetailedDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 3107372573469619607L;

    /**
     * 商品编号
     */
    private Integer id;

    /**
     * 商品
     */
    private String goodsName;

    /**
     * 规格
     */
    private String spec;

    /**
     * 分类
     */
    private String cId;

    /**
     * 品牌
     */
    private String bId;

    /**
     * 单位
     */
    private String uId;

    /**
     * 采购数量
     */
    private Integer number;

    /**
     * 进货价
     */
    private String purchasingCost;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 已入库
     */
    private String storaged;

    /**
     * 待入库
     */
    private String storaging;

    /**
     * 供应商
     */
    private String sId;

    /**
     * 采购单号
     */
    private String pId;

    /**
     * 获取商品编号
     *
     * @return 商品编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置商品编号
     *
     * @param id 商品编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品
     *
     * @return 商品
     */
    public String getGoodsName() {
        return this.goodsName;
    }

    /**
     * 设置商品
     *
     * @param goodsName 商品
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 获取规格
     *
     * @return 规格
     */
    public String getSpec() {
        return this.spec;
    }

    /**
     * 设置规格
     *
     * @param spec 规格
     */
    public void setSpec(String spec) {
        this.spec = spec;
    }

    /**
     * 获取分类
     *
     * @return 分类
     */
    public String getCId() {
        return this.cId;
    }

    /**
     * 设置分类
     *
     * @param cId 分类
     */
    public void setCId(String cId) {
        this.cId = cId;
    }

    /**
     * 获取品牌
     *
     * @return 品牌
     */
    public String getBId() {
        return this.bId;
    }

    /**
     * 设置品牌
     *
     * @param bId 品牌
     */
    public void setBId(String bId) {
        this.bId = bId;
    }

    /**
     * 获取单位
     *
     * @return 单位
     */
    public String getUId() {
        return this.uId;
    }

    /**
     * 设置单位
     *
     * @param uId 单位
     */
    public void setUId(String uId) {
        this.uId = uId;
    }

    /**
     * 获取采购数量
     *
     * @return 采购数量
     */
    public Integer getNumber() {
        return this.number;
    }

    /**
     * 设置采购数量
     *
     * @param number 采购数量
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 获取进货价
     *
     * @return 进货价
     */
    public String getPurchasingCost() {
        return this.purchasingCost;
    }

    /**
     * 设置进货价
     *
     * @param purchasingCost 进货价
     */
    public void setPurchasingCost(String purchasingCost) {
        this.purchasingCost = purchasingCost;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取已入库
     *
     * @return 已入库
     */
    public String getStoraged() {
        return this.storaged;
    }

    /**
     * 设置已入库
     *
     * @param storaged 已入库
     */
    public void setStoraged(String storaged) {
        this.storaged = storaged;
    }

    /**
     * 获取待入库
     *
     * @return 待入库
     */
    public String getStoraging() {
        return this.storaging;
    }

    /**
     * 设置待入库
     *
     * @param storaging 待入库
     */
    public void setStoraging(String storaging) {
        this.storaging = storaging;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSId() {
        return this.sId;
    }

    /**
     * 设置供应商
     *
     * @param sId 供应商
     */
    public void setSId(String sId) {
        this.sId = sId;
    }

    /**
     * 获取采购单号
     *
     * @return 采购单号
     */
    public String getPId() {
        return this.pId;
    }

    /**
     * 设置采购单号
     *
     * @param pId 采购单号
     */
    public void setPId(String pId) {
        this.pId = pId;
    }
}