package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 退货单表(BACK)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class BackDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 6178397815787111051L;

    /**  */
    private Integer id;

    /**
     * 退货单号
     */
    private String backId;

    /**
     * 退货时间
     */
    private String backTime;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 退款金额
     */
    private BigDecimal amount;

    /**
     * 退款状态(1:待审核,2:待发货,3:待收货,4:待退款,5:已完成,6:已取消)
     */
    private String backStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 内部沟通
     */
    private String rapport;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取退货单号
     *
     * @return 退货单号
     */
    public String getBackId() {
        return this.backId;
    }

    /**
     * 设置退货单号
     *
     * @param backId 退货单号
     */
    public void setBackId(String backId) {
        this.backId = backId;
    }

    /**
     * 获取退货时间
     *
     * @return 退货时间
     */
    public String getBackTime() {
        return this.backTime;
    }

    /**
     * 设置退货时间
     *
     * @param backTime 退货时间
     */
    public void setBackTime(String backTime) {
        this.backTime = backTime;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取退款金额
     *
     * @return 退款金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置退款金额
     *
     * @param amount 退款金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取退款状态(1:待审核,2:待发货,3:待收货,4:待退款,5:已完成,6:已取消)
     *
     * @return 退款状态(1 : 待审核
     */
    public String getBackStatus() {
        return this.backStatus;
    }

    /**
     * 设置退款状态(1:待审核,2:待发货,3:待收货,4:待退款,5:已完成,6:已取消)
     *
     * @param backStatus 退款状态(1:待审核,2:待发货,3:待收货,4:待退款,5:已完成,6:已取消)
     */
    public void setBackStatus(String backStatus) {
        this.backStatus = backStatus;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取内部沟通
     *
     * @return 内部沟通
     */
    public String getRapport() {
        return this.rapport;
    }

    /**
     * 设置内部沟通
     *
     * @param rapport 内部沟通
     */
    public void setRapport(String rapport) {
        this.rapport = rapport;
    }
}