package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 入库单表(STORAG)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class StoragDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -7279472216704618470L;

    /**  */
    private Integer id;

    /**
     * 入库单号
     */
    private String sId;

    /**
     * 入库时间
     */
    private String sTime;

    /**
     * 入库仓库
     */
    private String putInStorage;

    /**
     * 入库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     */
    private String sType;

    /**
     * 经办人
     */
    private String agent;

    /**
     * 供应商
     */
    private String supplier;

    /**
     * 审核状态(1:待审核,2:已审核,0:已取消)
     */
    private String sStatus;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取入库单号
     *
     * @return 入库单号
     */
    public String getSId() {
        return this.sId;
    }

    /**
     * 设置入库单号
     *
     * @param sId 入库单号
     */
    public void setSId(String sId) {
        this.sId = sId;
    }

    /**
     * 获取入库时间
     *
     * @return 入库时间
     */
    public String getSTime() {
        return this.sTime;
    }

    /**
     * 设置入库时间
     *
     * @param sTime 入库时间
     */
    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    /**
     * 获取入库仓库
     *
     * @return 入库仓库
     */
    public String getPutInStorage() {
        return this.putInStorage;
    }

    /**
     * 设置入库仓库
     *
     * @param putInStorage 入库仓库
     */
    public void setPutInStorage(String putInStorage) {
        this.putInStorage = putInStorage;
    }

    /**
     * 获取入库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     *
     * @return 入库类型(1 : 退货入库
     */
    public String getSType() {
        return this.sType;
    }

    /**
     * 设置入库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     *
     * @param sType 入库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     */
    public void setSType(String sType) {
        this.sType = sType;
    }

    /**
     * 获取经办人
     *
     * @return 经办人
     */
    public String getAgent() {
        return this.agent;
    }

    /**
     * 设置经办人
     *
     * @param agent 经办人
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSupplier() {
        return this.supplier;
    }

    /**
     * 设置供应商
     *
     * @param supplier 供应商
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * 获取审核状态(1:待审核,2:已审核,0:已取消)
     *
     * @return 审核状态(1 : 待审核
     */
    public String getSStatus() {
        return this.sStatus;
    }

    /**
     * 设置审核状态(1:待审核,2:已审核,0:已取消)
     *
     * @param sStatus 审核状态(1:待审核,2:已审核,0:已取消)
     */
    public void setSStatus(String sStatus) {
        this.sStatus = sStatus;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}