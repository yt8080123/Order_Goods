package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 资金流水表(FUND_STREAM)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class FundStreamDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -2511751775491296539L;

    /**  */
    private Integer id;

    /**
     * 收/付款单号
     */
    private String number;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 关联单号
     */
    private String arId;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 支付方式(1:转账支付,2:预付款支付)
     */
    private String payMode;

    /**
     * 开户银行
     */
    private String bank;

    /**
     * 银行账号
     */
    private String bankAccount;

    /**
     * 状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取收/付款单号
     *
     * @return 收/付款单号
     */
    public String getNumber() {
        return this.number;
    }

    /**
     * 设置收/付款单号
     *
     * @param number 收/付款单号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取关联单号
     *
     * @return 关联单号
     */
    public String getArId() {
        return this.arId;
    }

    /**
     * 设置关联单号
     *
     * @param arId 关联单号
     */
    public void setArId(String arId) {
        this.arId = arId;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取支付方式(1:转账支付,2:预付款支付)
     *
     * @return 支付方式(1 : 转账支付
     */
    public String getPayMode() {
        return this.payMode;
    }

    /**
     * 设置支付方式(1:转账支付,2:预付款支付)
     *
     * @param payMode 支付方式(1:转账支付,2:预付款支付)
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    /**
     * 获取开户银行
     *
     * @return 开户银行
     */
    public String getBank() {
        return this.bank;
    }

    /**
     * 设置开户银行
     *
     * @param bank 开户银行
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * 获取银行账号
     *
     * @return 银行账号
     */
    public String getBankAccount() {
        return this.bankAccount;
    }

    /**
     * 设置银行账号
     *
     * @param bankAccount 银行账号
     */
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * 获取状态
     *
     * @return 状态
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}