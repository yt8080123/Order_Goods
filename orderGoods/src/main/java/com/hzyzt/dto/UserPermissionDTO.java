package com.hzyzt.dto;

import lombok.Data;

/**
 * 用户权限(USER_PERMISSION)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class UserPermissionDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 4093843529412809299L;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 权限名
     */
    private String permissName;

    /**
     * 获取角色名
     *
     * @return 角色名
     */
    public String getRoleName() {
        return this.roleName;
    }

    /**
     * 设置角色名
     *
     * @param roleName 角色名
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取权限名
     *
     * @return 权限名
     */
    public String getPermissName() {
        return this.permissName;
    }

    /**
     * 设置权限名
     *
     * @param permissName 权限名
     */
    public void setPermissName(String permissName) {
        this.permissName = permissName;
    }
}