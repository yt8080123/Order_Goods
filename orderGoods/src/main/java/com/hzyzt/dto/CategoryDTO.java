package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品分类表(CATEGORY)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class CategoryDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -8214716534794848845L;

    /**
     * 分类编号
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String cName;

    /**
     * 排序
     */
    private String sort;

    /**
     * 获取分类编号
     *
     * @return 分类编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置分类编号
     *
     * @param id 分类编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取分类名称
     *
     * @return 分类名称
     */
    public String getCName() {
        return this.cName;
    }

    /**
     * 设置分类名称
     *
     * @param cName 分类名称
     */
    public void setCName(String cName) {
        this.cName = cName;
    }

    /**
     * 获取排序
     *
     * @return 排序
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
}