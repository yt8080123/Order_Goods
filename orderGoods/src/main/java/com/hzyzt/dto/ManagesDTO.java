package com.hzyzt.dto;

import lombok.Data;

/**
 * 补货单表(MANAGES)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class ManagesDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -6908083756081995831L;

    /**  */
    private Integer id;

    /**
     * 补货单
     */
    private String mId;

    /**
     * 时间
     */
    private String time;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 制单人
     */
    private String person;

    /**
     * 商品种数
     */
    private String kindNum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取补货单
     *
     * @return 补货单
     */
    public String getMId() {
        return this.mId;
    }

    /**
     * 设置补货单
     *
     * @param mId 补货单
     */
    public void setMId(String mId) {
        this.mId = mId;
    }

    /**
     * 获取时间
     *
     * @return 时间
     */
    public String getTime() {
        return this.time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 获取仓库
     *
     * @return 仓库
     */
    public String getWarehouse() {
        return this.warehouse;
    }

    /**
     * 设置仓库
     *
     * @param warehouse 仓库
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * 获取制单人
     *
     * @return 制单人
     */
    public String getPerson() {
        return this.person;
    }

    /**
     * 设置制单人
     *
     * @param person 制单人
     */
    public void setPerson(String person) {
        this.person = person;
    }

    /**
     * 获取商品种数
     *
     * @return 商品种数
     */
    public String getKindNum() {
        return this.kindNum;
    }

    /**
     * 设置商品种数
     *
     * @param kindNum 商品种数
     */
    public void setKindNum(String kindNum) {
        this.kindNum = kindNum;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}