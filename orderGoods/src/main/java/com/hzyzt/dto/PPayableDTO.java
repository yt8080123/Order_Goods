package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 采购应付款表(P_PAYABLE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class PPayableDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -3483695635268615371L;

    /**  */
    private Integer id;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 采购单
     */
    private String pOrder;

    /**
     * 采购员
     */
    private String buyer;

    /**
     * 采购单日期
     */
    private String pOrderDate;

    /**
     * 应付金额
     */
    private BigDecimal dueAmount;

    /**
     * 已付金额
     */
    private BigDecimal paidAmount;

    /**
     * 代付金额
     */
    private String tobeAmount;

    /**
     * 付款状态(1:待付款,2:部分付款,3:已付款,0:已取消)
     */
    private String payStatus;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取供应商名称
     *
     * @return 供应商名称
     */
    public String getSupplierName() {
        return this.supplierName;
    }

    /**
     * 设置供应商名称
     *
     * @param supplierName 供应商名称
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * 获取采购单
     *
     * @return 采购单
     */
    public String getPOrder() {
        return this.pOrder;
    }

    /**
     * 设置采购单
     *
     * @param pOrder 采购单
     */
    public void setPOrder(String pOrder) {
        this.pOrder = pOrder;
    }

    /**
     * 获取采购员
     *
     * @return 采购员
     */
    public String getBuyer() {
        return this.buyer;
    }

    /**
     * 设置采购员
     *
     * @param buyer 采购员
     */
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    /**
     * 获取采购单日期
     *
     * @return 采购单日期
     */
    public String getPOrderDate() {
        return this.pOrderDate;
    }

    /**
     * 设置采购单日期
     *
     * @param pOrderDate 采购单日期
     */
    public void setPOrderDate(String pOrderDate) {
        this.pOrderDate = pOrderDate;
    }

    /**
     * 获取应付金额
     *
     * @return 应付金额
     */
    public BigDecimal getDueAmount() {
        return this.dueAmount;
    }

    /**
     * 设置应付金额
     *
     * @param dueAmount 应付金额
     */
    public void setDueAmount(BigDecimal dueAmount) {
        this.dueAmount = dueAmount;
    }

    /**
     * 获取已付金额
     *
     * @return 已付金额
     */
    public BigDecimal getPaidAmount() {
        return this.paidAmount;
    }

    /**
     * 设置已付金额
     *
     * @param paidAmount 已付金额
     */
    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    /**
     * 获取代付金额
     *
     * @return 代付金额
     */
    public String getTobeAmount() {
        return this.tobeAmount;
    }

    /**
     * 设置代付金额
     *
     * @param tobeAmount 代付金额
     */
    public void setTobeAmount(String tobeAmount) {
        this.tobeAmount = tobeAmount;
    }

    /**
     * 获取付款状态(1:待付款,2:部分付款,3:已付款,0:已取消)
     *
     * @return 付款状态(1 : 待付款
     */
    public String getPayStatus() {
        return this.payStatus;
    }

    /**
     * 设置付款状态(1:待付款,2:部分付款,3:已付款,0:已取消)
     *
     * @param payStatus 付款状态(1:待付款,2:部分付款,3:已付款,0:已取消)
     */
    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }
}