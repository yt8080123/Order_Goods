package com.hzyzt.dto;

import lombok.Data;

/**
 * 店铺表(SHOP)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class ShopDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 5244815095960138934L;

    /**  */
    private Integer id;

    /**
     * 客户编号
     */
    private String customerId;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 付款状态（0未付款.1已付款）
     */
    private String paymentStatus;

    /**
     * 店铺认证（0未认证 1认证未通过 2已认证 ）
     */
    private String shopAuthentication;

    /**
     * 店铺剩余时间
     */
    private String shopRemainingTime;

    /**
     * 店铺注册时间
     */
    private String shopRegisterTime;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取客户编号
     *
     * @return 客户编号
     */
    public String getCustomerId() {
        return this.customerId;
    }

    /**
     * 设置客户编号
     *
     * @param customerId 客户编号
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * 获取店铺名称
     *
     * @return 店铺名称
     */
    public String getShopName() {
        return this.shopName;
    }

    /**
     * 设置店铺名称
     *
     * @param shopName 店铺名称
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /**
     * 获取付款状态（0未付款.1已付款）
     *
     * @return 付款状态（0未付款
     */
    public String getPaymentStatus() {
        return this.paymentStatus;
    }

    /**
     * 设置付款状态（0未付款.1已付款）
     *
     * @param paymentStatus 付款状态（0未付款.1已付款）
     */
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     * 获取店铺认证（0未认证 1认证未通过 2已认证 ）
     *
     * @return 店铺认证（0未认证 1认证未通过 2已认证 ）
     */
    public String getShopAuthentication() {
        return this.shopAuthentication;
    }

    /**
     * 设置店铺认证（0未认证 1认证未通过 2已认证 ）
     *
     * @param shopAuthentication 店铺认证（0未认证 1认证未通过 2已认证 ）
     */
    public void setShopAuthentication(String shopAuthentication) {
        this.shopAuthentication = shopAuthentication;
    }

    /**
     * 获取店铺剩余时间
     *
     * @return 店铺剩余时间
     */
    public String getShopRemainingTime() {
        return this.shopRemainingTime;
    }

    /**
     * 设置店铺剩余时间
     *
     * @param shopRemainingTime 店铺剩余时间
     */
    public void setShopRemainingTime(String shopRemainingTime) {
        this.shopRemainingTime = shopRemainingTime;
    }

    /**
     * 获取店铺注册时间
     *
     * @return 店铺注册时间
     */
    public String getShopRegisterTime() {
        return this.shopRegisterTime;
    }

    /**
     * 设置店铺注册时间
     *
     * @param shopRegisterTime 店铺注册时间
     */
    public void setShopRegisterTime(String shopRegisterTime) {
        this.shopRegisterTime = shopRegisterTime;
    }
}