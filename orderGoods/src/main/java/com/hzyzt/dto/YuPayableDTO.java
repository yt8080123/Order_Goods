package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 预付款表(YU_PAYABLE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class YuPayableDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 2119952979308232368L;

    /**  */
    private Integer id;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 联系人姓名
     */
    private String contactsName;

    /**
     * 预付款金额
     */
    private BigDecimal yuAmount;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取供应商名称
     *
     * @return 供应商名称
     */
    public String getSupplierName() {
        return this.supplierName;
    }

    /**
     * 设置供应商名称
     *
     * @param supplierName 供应商名称
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * 获取联系人姓名
     *
     * @return 联系人姓名
     */
    public String getContactsName() {
        return this.contactsName;
    }

    /**
     * 设置联系人姓名
     *
     * @param contactsName 联系人姓名
     */
    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    /**
     * 获取预付款金额
     *
     * @return 预付款金额
     */
    public BigDecimal getYuAmount() {
        return this.yuAmount;
    }

    /**
     * 设置预付款金额
     *
     * @param yuAmount 预付款金额
     */
    public void setYuAmount(BigDecimal yuAmount) {
        this.yuAmount = yuAmount;
    }
}