package com.hzyzt.dto;

import lombok.Data;

/**
 * 用户信息(USER)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class UserDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 8752447992247392826L;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户登录名
     */
    private String userLoginName;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 用户身份证号码
     */
    private String userIdcard;

    /**
     * 用户微信
     */
    private String userWechat;

    /**
     * 用户支付宝
     */
    private String userAlipay;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 修改日期
     */
    private String updateTime;

    /**
     * 用户类型（A:管理员用户；B:站点用户）
     */
    private String userType;

    /**
     * 获取用户id
     *
     * @return 用户id
     */
    public Integer getUserId() {
        return this.userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取用户名
     *
     * @return 用户名
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取用户登录名
     *
     * @return 用户登录名
     */
    public String getUserLoginName() {
        return this.userLoginName;
    }

    /**
     * 设置用户登录名
     *
     * @param userLoginName 用户登录名
     */
    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public String getUserPassword() {
        return this.userPassword;
    }

    /**
     * 设置密码
     *
     * @param userPassword 密码
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * 获取用户手机号
     *
     * @return 用户手机号
     */
    public String getUserPhone() {
        return this.userPhone;
    }

    /**
     * 设置用户手机号
     *
     * @param userPhone 用户手机号
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 获取用户身份证号码
     *
     * @return 用户身份证号码
     */
    public String getUserIdcard() {
        return this.userIdcard;
    }

    /**
     * 设置用户身份证号码
     *
     * @param userIdcard 用户身份证号码
     */
    public void setUserIdcard(String userIdcard) {
        this.userIdcard = userIdcard;
    }

    /**
     * 获取用户微信
     *
     * @return 用户微信
     */
    public String getUserWechat() {
        return this.userWechat;
    }

    /**
     * 设置用户微信
     *
     * @param userWechat 用户微信
     */
    public void setUserWechat(String userWechat) {
        this.userWechat = userWechat;
    }

    /**
     * 获取用户支付宝
     *
     * @return 用户支付宝
     */
    public String getUserAlipay() {
        return this.userAlipay;
    }

    /**
     * 设置用户支付宝
     *
     * @param userAlipay 用户支付宝
     */
    public void setUserAlipay(String userAlipay) {
        this.userAlipay = userAlipay;
    }

    /**
     * 获取创建时间
     *
     * @return 创建时间
     */
    public String getCreateTime() {
        return this.createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改日期
     *
     * @return 修改日期
     */
    public String getUpdateTime() {
        return this.updateTime;
    }

    /**
     * 设置修改日期
     *
     * @param updateTime 修改日期
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取用户类型（A:管理员用户；B:站点用户）
     *
     * @return 用户类型（A
     */
    public String getUserType() {
        return this.userType;
    }

    /**
     * 设置用户类型（A:管理员用户；B:站点用户）
     *
     * @param userType 用户类型（A:管理员用户；B:站点用户）
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }
}