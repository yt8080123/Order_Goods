package com.hzyzt.dto;

import lombok.Data;

/**
 * 序列(SEQ_QUENCE)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class SeqQuenceDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -6102552153631820102L;

    /**
     * id
     */
    private Integer seqId;

    /**
     * 表名
     */
    private String tablesName;

    /**
     * 序列
     */
    private Integer seqNum;

    /**
     * 日期
     */
    private String seqDate;

    /**
     * 获取id
     *
     * @return id
     */
    public Integer getSeqId() {
        return this.seqId;
    }

    /**
     * 设置id
     *
     * @param seqId id
     */
    public void setSeqId(Integer seqId) {
        this.seqId = seqId;
    }

    /**
     * 获取表名
     *
     * @return 表名
     */
    public String getTablesName() {
        return this.tablesName;
    }

    /**
     * 设置表名
     *
     * @param tablesName 表名
     */
    public void setTablesName(String tablesName) {
        this.tablesName = tablesName;
    }

    /**
     * 获取序列
     *
     * @return 序列
     */
    public Integer getSeqNum() {
        return this.seqNum;
    }

    /**
     * 设置序列
     *
     * @param seqNum 序列
     */
    public void setSeqNum(Integer seqNum) {
        this.seqNum = seqNum;
    }

    /**
     * 获取日期
     *
     * @return 日期
     */
    public String getSeqDate() {
        return this.seqDate;
    }

    /**
     * 设置日期
     *
     * @param seqDate 日期
     */
    public void setSeqDate(String seqDate) {
        this.seqDate = seqDate;
    }
}