package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 采购退货单表(PURCHASER_RETURN)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class PurchaserReturnDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -1374187815683196180L;

    /**
     * 退货单号
     */
    private Integer id;

    /**
     * 退货单日期
     */
    private String rDate;

    /**
     * 供应商
     */
    private String supplier;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 状态(1:待出库,2:已取消,3:已退款,4:已完成)
     */
    private String rStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 内部沟通
     */
    private String rapport;

    /**
     * 获取退货单号
     *
     * @return 退货单号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置退货单号
     *
     * @param id 退货单号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取退货单日期
     *
     * @return 退货单日期
     */
    public String getRDate() {
        return this.rDate;
    }

    /**
     * 设置退货单日期
     *
     * @param rDate 退货单日期
     */
    public void setRDate(String rDate) {
        this.rDate = rDate;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSupplier() {
        return this.supplier;
    }

    /**
     * 设置供应商
     *
     * @param supplier 供应商
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取状态(1:待出库,2:已取消,3:已退款,4:已完成)
     *
     * @return 状态(1 : 待出库
     */
    public String getRStatus() {
        return this.rStatus;
    }

    /**
     * 设置状态(1:待出库,2:已取消,3:已退款,4:已完成)
     *
     * @param rStatus 状态(1:待出库,2:已取消,3:已退款,4:已完成)
     */
    public void setRStatus(String rStatus) {
        this.rStatus = rStatus;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取内部沟通
     *
     * @return 内部沟通
     */
    public String getRapport() {
        return this.rapport;
    }

    /**
     * 设置内部沟通
     *
     * @param rapport 内部沟通
     */
    public void setRapport(String rapport) {
        this.rapport = rapport;
    }
}