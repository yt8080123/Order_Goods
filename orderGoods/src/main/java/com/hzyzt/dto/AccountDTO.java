package com.hzyzt.dto;

import lombok.Data;

/**
 * 收付款账户表(ACCOUNT)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class AccountDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -3670669433100914898L;

    /**  */
    private Integer id;

    /**
     * 账户名称
     */
    private String aName;

    /**
     * 开户银行
     */
    private String bank;

    /**
     * 银行账号
     */
    private String bankAccount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取账户名称
     *
     * @return 账户名称
     */
    public String getAName() {
        return this.aName;
    }

    /**
     * 设置账户名称
     *
     * @param aName 账户名称
     */
    public void setAName(String aName) {
        this.aName = aName;
    }

    /**
     * 获取开户银行
     *
     * @return 开户银行
     */
    public String getBank() {
        return this.bank;
    }

    /**
     * 设置开户银行
     *
     * @param bank 开户银行
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * 获取银行账号
     *
     * @return 银行账号
     */
    public String getBankAccount() {
        return this.bankAccount;
    }

    /**
     * 设置银行账号
     *
     * @param bankAccount 银行账号
     */
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}