package com.hzyzt.dto;

import lombok.Data;

/**
 * 收支明细表(BALANCE_DETAIL)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class BalanceDetailDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 7864125280584186560L;

    /**  */
    private Integer id;

    /**
     * 收支时间
     */
    private String bTime;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 收支类型(1:充值,2:退货退款,3:订单付款,4:期初充值,5:期初扣款,6:转预存款)
     */
    private String bType;

    /**
     * 支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     */
    private String payMode;

    /**
     * 收入
     */
    private String income;

    /**
     * 支出
     */
    private String defr;

    /**
     * 预存款金额
     */
    private String depositAmount;

    /**
     * 关联单号
     */
    private String arId;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取收支时间
     *
     * @return 收支时间
     */
    public String getBTime() {
        return this.bTime;
    }

    /**
     * 设置收支时间
     *
     * @param bTime 收支时间
     */
    public void setBTime(String bTime) {
        this.bTime = bTime;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取收支类型(1:充值,2:退货退款,3:订单付款,4:期初充值,5:期初扣款,6:转预存款)
     *
     * @return 收支类型(1 : 充值
     */
    public String getBType() {
        return this.bType;
    }

    /**
     * 设置收支类型(1:充值,2:退货退款,3:订单付款,4:期初充值,5:期初扣款,6:转预存款)
     *
     * @param bType 收支类型(1:充值,2:退货退款,3:订单付款,4:期初充值,5:期初扣款,6:转预存款)
     */
    public void setBType(String bType) {
        this.bType = bType;
    }

    /**
     * 获取支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     *
     * @return 支付方式(1 : 支付宝支付
     */
    public String getPayMode() {
        return this.payMode;
    }

    /**
     * 设置支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     *
     * @param payMode 支付方式(1:支付宝支付,2:快捷支付,3:微信支付,4:转账支付,5:预存款支付,6:货到付款,7:赊销支付,8:返利支付)
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    /**
     * 获取收入
     *
     * @return 收入
     */
    public String getIncome() {
        return this.income;
    }

    /**
     * 设置收入
     *
     * @param income 收入
     */
    public void setIncome(String income) {
        this.income = income;
    }

    /**
     * 获取支出
     *
     * @return 支出
     */
    public String getDefr() {
        return this.defr;
    }

    /**
     * 设置支出
     *
     * @param defr 支出
     */
    public void setDefr(String defr) {
        this.defr = defr;
    }

    /**
     * 获取预存款金额
     *
     * @return 预存款金额
     */
    public String getDepositAmount() {
        return this.depositAmount;
    }

    /**
     * 设置预存款金额
     *
     * @param depositAmount 预存款金额
     */
    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    /**
     * 获取关联单号
     *
     * @return 关联单号
     */
    public String getArId() {
        return this.arId;
    }

    /**
     * 设置关联单号
     *
     * @param arId 关联单号
     */
    public void setArId(String arId) {
        this.arId = arId;
    }
}