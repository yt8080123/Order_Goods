package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 出库单表(DELIVERY)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class DeliveryDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 1923262620048353780L;

    /**  */
    private Integer id;

    /**
     * 出库单号
     */
    private String dId;

    /**
     * 出库时间
     */
    private String dTime;

    /**
     * 关联订单
     */
    private String oId;

    /**
     * 出库仓库
     */
    private String output;

    /**
     * 出库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     */
    private String dType;

    /**
     * 经办人
     */
    private String agent;

    /**
     * 供应商
     */
    private String supplier;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取出库单号
     *
     * @return 出库单号
     */
    public String getDId() {
        return this.dId;
    }

    /**
     * 设置出库单号
     *
     * @param dId 出库单号
     */
    public void setDId(String dId) {
        this.dId = dId;
    }

    /**
     * 获取出库时间
     *
     * @return 出库时间
     */
    public String getDTime() {
        return this.dTime;
    }

    /**
     * 设置出库时间
     *
     * @param dTime 出库时间
     */
    public void setDTime(String dTime) {
        this.dTime = dTime;
    }

    /**
     * 获取关联订单
     *
     * @return 关联订单
     */
    public String getOId() {
        return this.oId;
    }

    /**
     * 设置关联订单
     *
     * @param oId 关联订单
     */
    public void setOId(String oId) {
        this.oId = oId;
    }

    /**
     * 获取出库仓库
     *
     * @return 出库仓库
     */
    public String getOutput() {
        return this.output;
    }

    /**
     * 设置出库仓库
     *
     * @param output 出库仓库
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * 获取出库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     *
     * @return 出库类型(1 : 退货入库
     */
    public String getDType() {
        return this.dType;
    }

    /**
     * 设置出库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     *
     * @param dType 出库类型(1:退货入库,2:采购入库,3:盘赢入库,0:其他入库)
     */
    public void setDType(String dType) {
        this.dType = dType;
    }

    /**
     * 获取经办人
     *
     * @return 经办人
     */
    public String getAgent() {
        return this.agent;
    }

    /**
     * 设置经办人
     *
     * @param agent 经办人
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSupplier() {
        return this.supplier;
    }

    /**
     * 设置供应商
     *
     * @param supplier 供应商
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}