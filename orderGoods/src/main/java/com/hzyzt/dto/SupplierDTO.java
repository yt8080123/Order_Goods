package com.hzyzt.dto;

import lombok.Data;

/**
 * 供应商表(SUPPLIER)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class SupplierDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 7217332962890518045L;

    /**  */
    private Integer id;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 联系人姓名
     */
    private String contactsName;

    /**
     * 手机电话
     */
    private String mobile;

    /**
     * 座机
     */
    private String studio;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 开户名称
     */
    private String accountName;

    /**
     * 开户银行
     */
    private String accountBank;

    /**
     * 银行账号
     */
    private String bankNumber;

    /**
     * 开票抬头
     */
    private String rise;

    /**
     * 纳税人识别号
     */
    private String identifyNumber;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取供应商名称
     *
     * @return 供应商名称
     */
    public String getSupplierName() {
        return this.supplierName;
    }

    /**
     * 设置供应商名称
     *
     * @param supplierName 供应商名称
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * 获取联系地址
     *
     * @return 联系地址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 设置联系地址
     *
     * @param address 联系地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置备注
     *
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取联系人姓名
     *
     * @return 联系人姓名
     */
    public String getContactsName() {
        return this.contactsName;
    }

    /**
     * 设置联系人姓名
     *
     * @param contactsName 联系人姓名
     */
    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    /**
     * 获取手机电话
     *
     * @return 手机电话
     */
    public String getMobile() {
        return this.mobile;
    }

    /**
     * 设置手机电话
     *
     * @param mobile 手机电话
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取座机
     *
     * @return 座机
     */
    public String getStudio() {
        return this.studio;
    }

    /**
     * 设置座机
     *
     * @param studio 座机
     */
    public void setStudio(String studio) {
        this.studio = studio;
    }

    /**
     * 获取邮箱
     *
     * @return 邮箱
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取开户名称
     *
     * @return 开户名称
     */
    public String getAccountName() {
        return this.accountName;
    }

    /**
     * 设置开户名称
     *
     * @param accountName 开户名称
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * 获取开户银行
     *
     * @return 开户银行
     */
    public String getAccountBank() {
        return this.accountBank;
    }

    /**
     * 设置开户银行
     *
     * @param accountBank 开户银行
     */
    public void setAccountBank(String accountBank) {
        this.accountBank = accountBank;
    }

    /**
     * 获取银行账号
     *
     * @return 银行账号
     */
    public String getBankNumber() {
        return this.bankNumber;
    }

    /**
     * 设置银行账号
     *
     * @param bankNumber 银行账号
     */
    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    /**
     * 获取开票抬头
     *
     * @return 开票抬头
     */
    public String getRise() {
        return this.rise;
    }

    /**
     * 设置开票抬头
     *
     * @param rise 开票抬头
     */
    public void setRise(String rise) {
        this.rise = rise;
    }

    /**
     * 获取纳税人识别号
     *
     * @return 纳税人识别号
     */
    public String getIdentifyNumber() {
        return this.identifyNumber;
    }

    /**
     * 设置纳税人识别号
     *
     * @param identifyNumber 纳税人识别号
     */
    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }
}