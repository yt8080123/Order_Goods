package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 采购单表(PURCHASER)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class PurchaserDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 2076466506068695830L;

    /**
     * 采购单编号
     */
    private Integer id;

    /**
     * 采购单日期
     */
    private String pDate;

    /**
     * 供应商
     */
    private String supplier;

    /**
     * 采购员
     */
    private String buyer;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 订单状态（1:待审核,2:待入库,3:部分入库,4:已取消,5:已完成）
     */
    private String pStatus;

    /**
     * 付款状态(1:待付款,2:部分付款,3:已付款,4:已取消)
     */
    private String payStatus;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 获取采购单编号
     *
     * @return 采购单编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置采购单编号
     *
     * @param id 采购单编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取采购单日期
     *
     * @return 采购单日期
     */
    public String getPDate() {
        return this.pDate;
    }

    /**
     * 设置采购单日期
     *
     * @param pDate 采购单日期
     */
    public void setPDate(String pDate) {
        this.pDate = pDate;
    }

    /**
     * 获取供应商
     *
     * @return 供应商
     */
    public String getSupplier() {
        return this.supplier;
    }

    /**
     * 设置供应商
     *
     * @param supplier 供应商
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * 获取采购员
     *
     * @return 采购员
     */
    public String getBuyer() {
        return this.buyer;
    }

    /**
     * 设置采购员
     *
     * @param buyer 采购员
     */
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    /**
     * 获取仓库
     *
     * @return 仓库
     */
    public String getWarehouse() {
        return this.warehouse;
    }

    /**
     * 设置仓库
     *
     * @param warehouse 仓库
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * 获取订单状态（1:待审核,2:待入库,3:部分入库,4:已取消,5:已完成）
     *
     * @return 订单状态（1:待审核
     */
    public String getPStatus() {
        return this.pStatus;
    }

    /**
     * 设置订单状态（1:待审核,2:待入库,3:部分入库,4:已取消,5:已完成）
     *
     * @param pStatus 订单状态（1:待审核,2:待入库,3:部分入库,4:已取消,5:已完成）
     */
    public void setPStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    /**
     * 获取付款状态(1:待付款,2:部分付款,3:已付款,4:已取消)
     *
     * @return 付款状态(1 : 待付款
     */
    public String getPayStatus() {
        return this.payStatus;
    }

    /**
     * 设置付款状态(1:待付款,2:部分付款,3:已付款,4:已取消)
     *
     * @param payStatus 付款状态(1:待付款,2:部分付款,3:已付款,4:已取消)
     */
    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 获取金额
     *
     * @return 金额
     */
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}