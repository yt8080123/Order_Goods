package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 客户资金表(CUSTOMER_FUNDS)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class CustomerFundsDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 6132460758917708534L;

    /**  */
    private Integer id;

    /**
     * 客户名称
     */
    private String customName;

    /**
     * 联系人姓名
     */
    private String contactsName;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 客户类型
     */
    private String cType;

    /**
     * 业务员
     */
    private String clerk;

    /**
     * 归属地区
     */
    private String area;

    /**
     * 状态(1:已启用,2:已停用,3:待激活,4:待审核)
     */
    private String status;

    /**
     * 余额
     */
    private BigDecimal balance;

    /**  */
    private String sort;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取客户名称
     *
     * @return 客户名称
     */
    public String getCustomName() {
        return this.customName;
    }

    /**
     * 设置客户名称
     *
     * @param customName 客户名称
     */
    public void setCustomName(String customName) {
        this.customName = customName;
    }

    /**
     * 获取联系人姓名
     *
     * @return 联系人姓名
     */
    public String getContactsName() {
        return this.contactsName;
    }

    /**
     * 设置联系人姓名
     *
     * @param contactsName 联系人姓名
     */
    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    /**
     * 获取联系电话
     *
     * @return 联系电话
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * 设置联系电话
     *
     * @param phone 联系电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取客户类型
     *
     * @return 客户类型
     */
    public String getCType() {
        return this.cType;
    }

    /**
     * 设置客户类型
     *
     * @param cType 客户类型
     */
    public void setCType(String cType) {
        this.cType = cType;
    }

    /**
     * 获取业务员
     *
     * @return 业务员
     */
    public String getClerk() {
        return this.clerk;
    }

    /**
     * 设置业务员
     *
     * @param clerk 业务员
     */
    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    /**
     * 获取归属地区
     *
     * @return 归属地区
     */
    public String getArea() {
        return this.area;
    }

    /**
     * 设置归属地区
     *
     * @param area 归属地区
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * 获取状态(1:已启用,2:已停用,3:待激活,4:待审核)
     *
     * @return 状态(1 : 已启用
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * 设置状态(1:已启用,2:已停用,3:待激活,4:待审核)
     *
     * @param status 状态(1:已启用,2:已停用,3:待激活,4:待审核)
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取余额
     *
     * @return 余额
     */
    public BigDecimal getBalance() {
        return this.balance;
    }

    /**
     * 设置余额
     *
     * @param balance 余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * 获取
     *
     * @return
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置
     *
     * @param sort
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
}