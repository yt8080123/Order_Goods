package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品单位表(UNIT)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class UnitDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 6964288472570581702L;

    /**
     * 商品单位表
     */
    private Integer id;

    /**
     * 单位名称
     */
    private String unitName;

    /**
     * 排序
     */
    private String sort;

    /**
     * 获取商品单位表
     *
     * @return 商品单位表
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置商品单位表
     *
     * @param id 商品单位表
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取单位名称
     *
     * @return 单位名称
     */
    public String getUnitName() {
        return this.unitName;
    }

    /**
     * 设置单位名称
     *
     * @param unitName 单位名称
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * 获取排序
     *
     * @return 排序
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
}