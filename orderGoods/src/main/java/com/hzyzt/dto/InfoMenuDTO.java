package com.hzyzt.dto;

import lombok.Data;

/**
 * 菜单表(INFO_MENU)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class InfoMenuDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 4128012908067360543L;

    /**
     * 菜单ID
     */
    private String menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单级别
     */
    private String menuLevel;

    /**
     * 控制器（url）
     */
    private String menuUrl;

    /**
     * 父节点菜单
     */
    private String parentMenu;

    /**
     * 显示顺序
     */
    private Integer menuOrder;

    /**
     * 菜单显示图标
     */
    private String menuIcon;

    /**
     * 菜单权限
     */
    private String menuPermission;

    /**
     * 获取菜单ID
     *
     * @return 菜单ID
     */
    public String getMenuId() {
        return this.menuId;
    }

    /**
     * 设置菜单ID
     *
     * @param menuId 菜单ID
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取菜单名称
     *
     * @return 菜单名称
     */
    public String getMenuName() {
        return this.menuName;
    }

    /**
     * 设置菜单名称
     *
     * @param menuName 菜单名称
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * 获取菜单级别
     *
     * @return 菜单级别
     */
    public String getMenuLevel() {
        return this.menuLevel;
    }

    /**
     * 设置菜单级别
     *
     * @param menuLevel 菜单级别
     */
    public void setMenuLevel(String menuLevel) {
        this.menuLevel = menuLevel;
    }

    /**
     * 获取控制器（url）
     *
     * @return 控制器（url）
     */
    public String getMenuUrl() {
        return this.menuUrl;
    }

    /**
     * 设置控制器（url）
     *
     * @param menuUrl 控制器（url）
     */
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    /**
     * 获取父节点菜单
     *
     * @return 父节点菜单
     */
    public String getParentMenu() {
        return this.parentMenu;
    }

    /**
     * 设置父节点菜单
     *
     * @param parentMenu 父节点菜单
     */
    public void setParentMenu(String parentMenu) {
        this.parentMenu = parentMenu;
    }

    /**
     * 获取显示顺序
     *
     * @return 显示顺序
     */
    public Integer getMenuOrder() {
        return this.menuOrder;
    }

    /**
     * 设置显示顺序
     *
     * @param menuOrder 显示顺序
     */
    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    /**
     * 获取菜单显示图标
     *
     * @return 菜单显示图标
     */
    public String getMenuIcon() {
        return this.menuIcon;
    }

    /**
     * 设置菜单显示图标
     *
     * @param menuIcon 菜单显示图标
     */
    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    /**
     * 获取菜单权限
     *
     * @return 菜单权限
     */
    public String getMenuPermission() {
        return this.menuPermission;
    }

    /**
     * 设置菜单权限
     *
     * @param menuPermission 菜单权限
     */
    public void setMenuPermission(String menuPermission) {
        this.menuPermission = menuPermission;
    }
}