package com.hzyzt.dto;

import lombok.Data;

/**
 * 商品品牌表(BRAND)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class BrandDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -1142925446713501054L;

    /**
     * 品牌编号
     */
    private Integer id;

    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 排序码
     */
    private String sort;

    /**
     * 获取品牌编号
     *
     * @return 品牌编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置品牌编号
     *
     * @param id 品牌编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取品牌名称
     *
     * @return 品牌名称
     */
    public String getBrandName() {
        return this.brandName;
    }

    /**
     * 设置品牌名称
     *
     * @param brandName 品牌名称
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * 获取排序码
     *
     * @return 排序码
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * 设置排序码
     *
     * @param sort 排序码
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
}