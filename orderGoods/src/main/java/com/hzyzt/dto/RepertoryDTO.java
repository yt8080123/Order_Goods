package com.hzyzt.dto;

import lombok.Data;

/**
 * 仓库表(REPERTORY)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class RepertoryDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -4928649378445669347L;

    /**  */
    private Integer id;

    /**
     * 仓库名称
     */
    private String rName;

    /**
     * 所在地区
     */
    private String region;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 仓库面积
     */
    private String area;

    /**
     * 负责人
     */
    private String header;

    /**
     * 联系电话
     */
    private String mobile;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 获取
     *
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取仓库名称
     *
     * @return 仓库名称
     */
    public String getRName() {
        return this.rName;
    }

    /**
     * 设置仓库名称
     *
     * @param rName 仓库名称
     */
    public void setRName(String rName) {
        this.rName = rName;
    }

    /**
     * 获取所在地区
     *
     * @return 所在地区
     */
    public String getRegion() {
        return this.region;
    }

    /**
     * 设置所在地区
     *
     * @param region 所在地区
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * 获取详细地址
     *
     * @return 详细地址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 设置详细地址
     *
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取仓库面积
     *
     * @return 仓库面积
     */
    public String getArea() {
        return this.area;
    }

    /**
     * 设置仓库面积
     *
     * @param area 仓库面积
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * 获取负责人
     *
     * @return 负责人
     */
    public String getHeader() {
        return this.header;
    }

    /**
     * 设置负责人
     *
     * @param header 负责人
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * 获取联系电话
     *
     * @return 联系电话
     */
    public String getMobile() {
        return this.mobile;
    }

    /**
     * 设置联系电话
     *
     * @param mobile 联系电话
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取备注
     *
     * @return 备注
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置备注
     *
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}