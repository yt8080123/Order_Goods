package com.hzyzt.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单应收款表(ORDER_AR)
 *
 * @author bianj
 * @version 1.0.0 2018-10-12
 */
@Data
public class OrderArDTO implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 4247865508567630224L;

    /**
     * 编号
     */
    private Integer arId;

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 订单时间
     */
    private String orderTime;

    /**
     * 应收金额
     */
    private BigDecimal arAmount;

    /**
     * 已收金额
     */
    private BigDecimal doneAmount;

    /**
     * 待收金额
     */
    private BigDecimal stayAmount;

    /**
     * 收款状态(1:未全部收款,2:已全部,0:全部)
     */
    private String arStatus;

    /**
     * 业务员
     */
    private String clerk;

    /**
     * 获取编号
     *
     * @return 编号
     */
    public Integer getArId() {
        return this.arId;
    }

    /**
     * 设置编号
     *
     * @param arId 编号
     */
    public void setArId(Integer arId) {
        this.arId = arId;
    }

    /**
     * 获取订单号
     *
     * @return 订单号
     */
    public String getOrderId() {
        return this.orderId;
    }

    /**
     * 设置订单号
     *
     * @param orderId 订单号
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单时间
     *
     * @return 订单时间
     */
    public String getOrderTime() {
        return this.orderTime;
    }

    /**
     * 设置订单时间
     *
     * @param orderTime 订单时间
     */
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    /**
     * 获取应收金额
     *
     * @return 应收金额
     */
    public BigDecimal getArAmount() {
        return this.arAmount;
    }

    /**
     * 设置应收金额
     *
     * @param arAmount 应收金额
     */
    public void setArAmount(BigDecimal arAmount) {
        this.arAmount = arAmount;
    }

    /**
     * 获取已收金额
     *
     * @return 已收金额
     */
    public BigDecimal getDoneAmount() {
        return this.doneAmount;
    }

    /**
     * 设置已收金额
     *
     * @param doneAmount 已收金额
     */
    public void setDoneAmount(BigDecimal doneAmount) {
        this.doneAmount = doneAmount;
    }

    /**
     * 获取待收金额
     *
     * @return 待收金额
     */
    public BigDecimal getStayAmount() {
        return this.stayAmount;
    }

    /**
     * 设置待收金额
     *
     * @param stayAmount 待收金额
     */
    public void setStayAmount(BigDecimal stayAmount) {
        this.stayAmount = stayAmount;
    }

    /**
     * 获取收款状态(1:未全部收款,2:已全部,0:全部)
     *
     * @return 收款状态(1 : 未全部收款
     */
    public String getArStatus() {
        return this.arStatus;
    }

    /**
     * 设置收款状态(1:未全部收款,2:已全部,0:全部)
     *
     * @param arStatus 收款状态(1:未全部收款,2:已全部,0:全部)
     */
    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }

    /**
     * 获取业务员
     *
     * @return 业务员
     */
    public String getClerk() {
        return this.clerk;
    }

    /**
     * 设置业务员
     *
     * @param clerk 业务员
     */
    public void setClerk(String clerk) {
        this.clerk = clerk;
    }
}