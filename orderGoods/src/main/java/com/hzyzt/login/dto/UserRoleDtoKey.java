package com.hzyzt.login.dto;

public class UserRoleDtoKey {
    private String roleName;

    private Integer userId;

    public UserRoleDtoKey() {
    }

    public UserRoleDtoKey(String roleName, Integer userId) {
        this.roleName = roleName;
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}