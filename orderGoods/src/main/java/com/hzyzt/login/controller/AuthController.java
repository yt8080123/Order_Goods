package com.hzyzt.login.controller;

import com.common.constant.ApplicationConstant;
import com.common.dto.ResultDto;
import com.common.dto.ResultDtoFactory;
import com.common.security.SecurityContext;
import com.common.util.StackTraceUtil;
import com.common.util.WebUtil;
import com.hzyzt.user.dto.MenuInfoDto;
import com.hzyzt.user.dto.UserDto;
import com.hzyzt.user.service.MenuInfoService;
import com.hzyzt.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description : 首页.
 * @Author : 吴书新
 */
@Controller
@RequestMapping(value = "/auth")
@Slf4j
public class AuthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    private MenuInfoService menuInfoService;

    @Autowired
    private UserService userService;

    private static final String MODEL_MENU = "menu";

    private static final String MODEL_USERNAME = "userName";

    @RequestMapping(value = "/login")
    public String goLogin(Model model,HttpServletRequest request){
        return "login/login";
    }

    @RequestMapping(value = "doLogin",method = RequestMethod.POST)
    public String doLogin(HttpServletRequest request, HttpSession session, @RequestParam String userName,
                          @RequestParam String userPassword,
                          @RequestParam String loginType, Model model){
        String username = userName/*userDto.getUserName()*/;
        String password = userPassword/*userDto.getUserPassword()*/;
        UserDto userDto = null;
        try {
            userDto = userService.getUserByUserName(userName);
        } catch (Exception e) {
            LOGGER.error(StackTraceUtil.getStackTrace(e));
        }
        // 登陆
        try {
            SecurityContext.login(username, password,false, session,userDto);
        } catch (Exception e) {
            e.getStackTrace();
            return "../../500.html";
        }
        if("1".equals(loginType)){
            return "redirect:/auth/home";
        }else{
            return "redirect:/auth/phHome";
        }
    }

    @RequestMapping(value = "weixinLogin",method = RequestMethod.POST)
    @ResponseBody
    public ResultDto weixinLogin(HttpServletRequest request, HttpSession session, @RequestParam String userName,
                                 @RequestParam String userPassword,
                                 @RequestParam String loginType, Model model){
        String username = userName/*userDto.getUserName()*/;
        String password = userPassword/*userDto.getUserPassword()*/;
        UserDto userDto = null;
        try {
            userDto = userService.getUserByUserName(userName);
        } catch (Exception e) {
            LOGGER.error(StackTraceUtil.getStackTrace(e));
        }
        // 登陆
        try {
            SecurityContext.login(username, password,false, session,userDto);
        } catch (Exception e) {
            e.getStackTrace();
            return ResultDtoFactory.toNack("登录失败");
        }
        return ResultDtoFactory.toAck("登录成功",userDto);
    }

    @RequestMapping(value = "home",method = RequestMethod.GET)
    public String index(HttpServletRequest request,Model model,HttpSession httpSession){
        try {
            List<MenuInfoDto> menuInfoDtos = menuInfoService.getAllMenu();
            model.addAttribute(MODEL_MENU,menuInfoDtos);
            UserDto userDto = (UserDto) httpSession.getAttribute(ApplicationConstant.SESSIONUSER);
            model.addAttribute(MODEL_USERNAME,userDto.getUserName());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            model.addAttribute(MODEL_MENU,null);
        }
        model.addAttribute("user", WebUtil.getSessionUser());
        return "common/home";
    }

    @RequestMapping(value = "phHome",method = RequestMethod.GET)
    public String phIndex(HttpServletRequest request,Model model,HttpSession httpSession){

        return "/receive/phAddReceive";
    }

    @RequestMapping(value = "index")
    public String indexHtml(){
        return "login/login";
    }
}
