package com.hzyzt.login.dao;

import com.hzyzt.login.dto.UserRoleDtoKey;

public interface UserRoleDtoMapper {
    int deleteByPrimaryKey(UserRoleDtoKey key);

    int insert(UserRoleDtoKey record);

    int insertSelective(UserRoleDtoKey record);
}