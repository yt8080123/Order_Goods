package com.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author wenchaochen
 *
 * @param <T>
 */

@SuppressWarnings("serial")
public class PageObjectDto<T> implements Serializable {
	
	@JsonProperty(value = "page")
	private int pageNum;
	
	@JsonProperty(value = "total")
	private int total;
	
	@JsonProperty(value = "records")
	private long records;
	
	@JsonProperty(value = "rows")
    private List<T> data = new ArrayList<T>();
	
	private double max;
	
	private double min;
	
	private String status;

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public long getRecords() {
		return records;
	}

	public void setRecords(long l) {
		this.records = l;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PageProductDto [pageNum=" + pageNum + ", total=" + total
				+ ", records=" + records + ", data=" + data + "]";
	}
	
}
