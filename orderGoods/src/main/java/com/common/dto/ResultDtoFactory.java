package com.common.dto;

import com.common.constant.ResultCode;
import com.common.enums.EErrorCode;
import com.common.exception.BaseException;
import com.common.exception.DisplayableError;
import com.common.util.AppConfigUtil;
import com.common.util.MessageUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.validation.BindingResult;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class ResultDtoFactory {

    private ResultDtoFactory() {
    };


    public static ResultDto toAck(String msg) {
        return toAck(msg, null);
    }
    
 

    public static ResultDto toAck(String msg, Object data) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.ACK);
        dto.setMessage(msg);
        dto.setData(data);
        return dto;
    }


    public static ResultDto toRedirect(String url) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.REDIRECT);
        dto.setData(url);
        return dto;
    }


    public static ResultDto toNack(String msg) {
        return toNack(msg, null);
    }


    public static ResultDto toNack(DisplayableError error) {
        String msg = "";
        if (error != null && StringUtils.isNotBlank(error.getErrorCode())) {
            msg = MessageUtil.getMessage(error.getDisplayMsg(), error.getArgs());
        }
        return toNack(msg, null);
    }


    public static ResultDto toNack(String msg, Object data) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.NACK);
        dto.setMessage(msg);
        dto.setData(data);
        return dto;
    }


    public static ResultDto toValidationError(String msg, BindingResult br) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.VALIDATION_ERROR);
        dto.setMessage(msg);
        dto.setData(br);
        return dto;
    }

    private static ResultDto toCommonError(String code, String msg, String details) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.COMMON_ERROR);
        StringBuilder text = new StringBuilder();
        if (StringUtils.isBlank(msg)) {
            text.append(MessageUtil.getMessage(EErrorCode.COMM_INTERNAL_ERROR.getDisplayMsg())).append("[")
                    .append("时间：").append((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date())).append("]");
        } else {
            text.append(msg);
        }
        if (StringUtils.isNotBlank(code)) {
            text.append("(").append(code).append(")");
        }
        dto.setMessage(text.toString());
        if (!AppConfigUtil.isProdEnv()) {
            dto.setData(details);
        }
        return dto;
    }


    public static ResultDto toCommonError(BaseException e) {
        String msg = MessageUtil.getMessage(e.getError().getDisplayMsg());
        return toCommonError(e.getError().getErrorCode(), msg, ExceptionUtils.getStackTrace(e));
    }


    public static ResultDto toCommonError(Exception e) {
        return toCommonError(null, null, ExceptionUtils.getStackTrace(e));
    }


    public static ResultDto toCommonError(String msg) {
        return toCommonError(null, msg, null);
    }


    public static ResultDto toBizError(String msg, Exception e) {
        ResultDto dto = new ResultDto();
        dto.setCode(ResultCode.BUSINESS_ERROR);
        dto.setMessage(msg == null ? e.getMessage() : msg);
        if (!AppConfigUtil.isProdEnv()) {
            dto.setData(ExceptionUtils.getStackTrace(e));
        }
        return dto;
    }
}
