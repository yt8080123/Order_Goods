package com.common.dto;

import java.io.Serializable;

/**
* Class Name: PageInfoDto.
* Description: TODO
* @author SC
*
*/

public class PageInfoDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//@JsonProperty(value = "page")
	protected int page;
	
	//@JsonProperty(value = "rows")
	protected int rows;
	
	//@JsonProperty(value = "_search")
	protected boolean _search;
	
	//@JsonProperty(value = "nd")
	protected String nd;
	
	//@JsonProperty(value = "sidx")
	protected String sidx;
	
	//@JsonProperty(value = "sord")
	protected String sord;
	
	protected int rowno;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public boolean is_search() {
		return _search;
	}

	public void set_search(boolean search) {
		this._search = search;
	}

	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

    public int getRowno() {
        return rowno;
    }

    public void setRowno(int rowno) {
        this.rowno = rowno;
    }

    @Override
	public String toString() {
		return "PageInfoDto [page=" + page + ", rows=" + rows + ", _search="
				+ _search + ", nd=" + nd + ", sidx=" + sidx + ", sord=" + sord
				+ "]";
	}
	
}
