package com.common.service;


import com.common.entity.Goods;

import java.util.List;

public interface GoodsService {


	List<Goods> getGoodsList(int offset, int limit);


	void buyGoods(long userPhone, long goodsId, boolean useProcedure);

}
