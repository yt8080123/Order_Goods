package com.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Locale;
/**
 * 
 * @author fanghu
 *
 */
public class SpringContextUtil implements ApplicationContextAware {

	private static ApplicationContext context = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.context = applicationContext;
	}

	/** 
	 * @Title: getBean 
	 * @Description: 
	 * @Auth wentaolu
	 * @Date Jul 22, 2016 3:10:55 PM
	 * @param beanName 名字.
	 * @param <T> 正确
	 * @return  get 得到
	 * @throws 
	 */
	public static <T> T getBean(String beanName) {
		return (T) context.getBean(beanName);
	}

	/** 
	 * @Title: getMessage 
	 * @Description: 
	 * @Auth wentaolu
	 * @Date Jul 22, 2016 3:10:58 PM
	 * @param key 关键.
	 * @return get 得到
	 * @throws 
	 */
	public static String getMessage(String key) {
		return context.getMessage(key, null, Locale.getDefault());
	}

}
