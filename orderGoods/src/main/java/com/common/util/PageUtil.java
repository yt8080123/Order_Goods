package com.common.util;

import com.common.dto.PageInfoDto;
import com.common.dto.PageObjectDto;
import com.github.pagehelper.PageInfo;
import com.hzyzt.user.dto.MenuInfoDto;
import com.hzyzt.user.dto.UserDto;
import com.hzyzt.user.enums.UserTypeEnum;
import com.hzyzt.user.service.MenuInfoService;
import com.hzyzt.user.service.UserService;
import org.springframework.ui.Model;

import java.util.List;

public  class PageUtil {
	private static final String MODEL_MENU = "menu";
	/**
	 * 
	 * @param list
	 * @param e
	 * @return
	 */
	public static <E> PageObjectDto<E> getPageInfo(List<E> list, E e) {
		PageInfo<E> pageInfo = new PageInfo<E>(list);
		PageObjectDto<E> page = new PageObjectDto<E>();
		page.setData(list);
		page.setPageNum(((PageInfoDto) e).getPage());
		page.setRecords(pageInfo.getTotal());
		int total = 0;
		if (StringUtil.isListNull(list)) {
            total = 1;
        } else {
            if (pageInfo.getTotal() % ((PageInfoDto) e).getRows() == 0) {
                total = (int) (pageInfo.getTotal() / ((PageInfoDto) e)
                        .getRows());
            } else {
                total = (int) (pageInfo.getTotal() / ((PageInfoDto) e)
                        .getRows()) + 1;
            }
        }
		page.setTotal(total);
		return page;
	}

	/**
	 * @Title: PageTitleInfo
	 * @Description:页面title信息
	 * @User shuxinwu
	 * @Time 2015年5月27日 上午11:13:07
	 * @param model
	 * @param menuArray
	 * @return void
	 * @throws
	 */
	public static void pageTitleInfo(Model model, String[] menuArray) {
		model.addAttribute("menuArray", menuArray);
	}

	public static void modelAddAttr(Model model, MenuInfoService menuInfoService, UserService userService) {
		List<UserDto> userDtoList;
		try {
			List<MenuInfoDto> menuInfoDtos = menuInfoService.getAllMenu();
			model.addAttribute(MODEL_MENU,menuInfoDtos);
			userDtoList = userService.getUserAll(null, UserTypeEnum.B.getCode());
			model.addAttribute("userDtos",userDtoList);
		}catch (Exception e){
			model.addAttribute("userDtos",null);
		}
	}

}
