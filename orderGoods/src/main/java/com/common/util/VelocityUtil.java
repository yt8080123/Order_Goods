package com.common.util;

import com.hzyzt.user.dto.MenuInfoDto;
import com.hzyzt.user.dto.UserDto;
import com.hzyzt.user.service.MenuInfoService;
import com.hzyzt.user.service.impl.MenuInfoServiceImpl;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @author fanghu
 *
 */
public class VelocityUtil {

	@Resource
	private MenuInfoService menuInfoService;

	public List<MenuInfoDto> getMenu() {
		try {
			MenuInfoServiceImpl menuInfoServiceImpl = new MenuInfoServiceImpl();
			List<MenuInfoDto> menuInfoDtos = menuInfoServiceImpl.getAllMenu();
			return menuInfoDtos;
		}catch (Exception e){
			StackTraceUtil.getStackTrace(e);
			return null;
		}
	}

	public String getSessionUser(){
		UserDto userDto = WebUtil.getSessionUser();
		return userDto.getUserName();
	}

}
