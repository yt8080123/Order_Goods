package com.common.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * Datetime ： 2012-7-11 下午4:40:31<br>
 * . Title : StringUtil.java<br>
 * Description: 字符串处理类<br>
 * Copyright : Copyright (c) 2012<br>
 * Company : <br>
 * 
 * @author zhangminhao
 *
 */
public  class StringUtil {
	
	private static final String BEIJING = "北京";
	private static final String TIANJIN = "天津";
	private static final String SHANGHAI = "上海";
	private static final String CHONGQIN = "重庆";
	private static final String SHENZHEN = "深圳";
	private static final int number2 = 2;
	private static final int number3 = 3;
	private static final int number4 = 4;
	private static final int number5 = 5;
	private static final int number8 = 8;
	private static final int number6 = 6;
	/***
	 * 字符串右填充（按字节）.
	 * 
	 * @param object
	 *            要填充的对象
	 * @param size
	 *            填充后的长度
	 * @param padChar
	 *            用什么字符填充
	 * @param charset
	 *            字符串编码
	 * @return 填充之后的字符串
	 * @throws UnsupportedEncodingException
	 */
	
	private static Logger logger = LoggerFactory.getLogger(StringUtil.class);
	/** 
	 * @Title: rpad 
	 * @Description: 
	 * @Auth wentaolu
	 * @Date Jul 22, 2016 3:13:02 PM
	 * @param object
	 * @param size
	 * @param padChar
	 * @param charset
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws 
	 */
	public static String rpad(Object object, int size, char padChar,
			String charset) throws UnsupportedEncodingException {
		String str = object == null ? "" : object.toString();

		byte[] bytes = new byte[size];
		byte[] strBytes = str.getBytes(charset);
		int strLenth = strBytes.length;

		for (int i = 0; i < bytes.length; i++) {
			if (i < strLenth) {
				bytes[i] = strBytes[i];
			} else {
				bytes[i] = (byte) padChar;
			}
		}
		return new String(bytes, charset);

	}

	/**
	 * 按字节对字符串进行截取.
	 * 
	 * @param str
	 *            要截取的字符串
	 * @param startIndex
	 *            开始索引(从0开始)
	 * @param endIndex
	 *            结束索引(从0开始,必须大于startIndex)
	 * @param charset
	 *            字符串编码方式
	 * @return 返回startIndex<= 字符串 < endIndex
	 * @throws UnsupportedEncodingException
	 */
	public static String getSubString(String str, int startIndex, int endIndex,
			String charset) throws UnsupportedEncodingException {
		if (str == null) {
			return null;
		}

		byte[] strBytes = str.getBytes(charset);
		if (startIndex >= strBytes.length) {
			return null;
		}

		if (endIndex <= startIndex) {
			return null;
		}

		byte[] bytes = new byte[strBytes.length];
		int j = 0;
		for (int i = startIndex; i < endIndex; i++) {
			if (i >= strBytes.length) {
				break;
			}

			bytes[j] = strBytes[i];
			j++;
		}

		return new String(bytes, charset);
	}

	/** 
	 * @Title: lPad 
	 * @Description: 
	 * @Auth wentaolu
	 * @Date Jul 22, 2016 3:13:20 PM
	 * @param object
	 * @param size
	 * @param padChar
	 * @param charset
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws 
	 */
	public static String lPad(Object object, int size, char padChar,
			String charset) throws UnsupportedEncodingException {
		String str = object == null ? "" : object.toString();
		byte[] bytes = new byte[size];
		byte[] strBytes = str.getBytes(charset);
		int strLenth = strBytes.length;

		int j = strLenth - 1;
		for (int i = bytes.length - 1; i >= 0; i--) {
			if (j >= 0) {
				bytes[i] = strBytes[j];
				j--;
			} else {
				bytes[i] = (byte) padChar;
			}
		}
		return new String(bytes, charset);

	}

	/**
	 * 校验实体类对象为空.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isObjNull(Object obj) {
		boolean isNull = false;
		if (null == obj) {
			isNull = true;
		}
		return isNull;
	}

	/**
	 * 校验实体类对象不为空.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isObjNotNull(Object obj) {
		boolean isNotNull = false;
		if (null != obj) {
			isNotNull = true;
		}
		return isNotNull;
	}

	/**
	 * 校验两字符串是否相同(不忽略大小写).
	 * 
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean isEqualObj(String obj1, String obj2) {
		boolean isEqual = false;
		if (isStrNullOrEmpty(obj1) && isStrNullOrEmpty(obj2)) {
			isEqual = true;
		} else {
			if (isObjNull(obj1)) {
				obj1 = "_hiwan";
			}

			if (obj1.equals(obj2)) {
				isEqual = true;
			}
		}

		return isEqual;
	}

	/**
	 * 校验两字符串是否相同(忽略大小写).
	 * 
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean isEqualObjIgnoreCase(String obj1, String obj2) {
		boolean isEqual = false;
		if (isStrNullOrEmpty(obj1) && isStrNullOrEmpty(obj2)) {
			isEqual = true;
		} else {
			if (isObjNull(obj1)) {
				obj1 = "_hiwan";
			}

			if (obj1.equalsIgnoreCase(obj2)) {
				isEqual = true;
			}
		}
		return isEqual;
	}

	/**
	 * 校验字段不为空.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isStrNotNullAndNotEmpty(String obj) {
		boolean isNot = false;
		if (null != obj && !"".equalsIgnoreCase(obj)) {
			isNot = true;
		}
		return isNot;
	}

	/**
	 * 校验字段为空.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isStrNullOrEmpty(String obj) {
		boolean isNull = false;
		if (null == obj || "".equalsIgnoreCase(obj)) {
			isNull = true;
		}
		return isNull;
	}

	/**
	 * 校验字段不为空且不为0.
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isStrNotNullAndNotEqualZero(String obj) {
		boolean isNotNull = false;
		if (isStrNotNullAndNotEmpty(obj) && !"0".equalsIgnoreCase(obj)) {
			isNotNull = true;
		}
		return isNotNull;
	}

	/**
	 * 校验集合不为空.
	 * 
	 * @param objList
	 * @return
	 */
	public static boolean isListNotNull(List<?> objList) {
		boolean isNot = false;
		if (null != objList && 0 != objList.size()) {
			isNot = true;
		}
		return isNot;
	}

	/**
	 * 校验集合为空.
	 * 
	 * @param objList
	 * @return
	 */
	public static boolean isListNull(List<?> objList) {
		boolean isNull = false;
		if (!isListNotNull(objList)) {
			isNull = true;
		}
		return isNull;
	}

	/**
	 * 校验字符串1是否包含字符串2.
	 * 
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean contains(String obj1, String obj2) {
		boolean isContains = false;
		if (obj1.contains(obj2)) {
			isContains = true;
		}
		return isContains;
	}

	/**
	 * 格式化数字(过期方法,停止使用).
	 * 
	 * @param dfValue
	 *            格式化数字
	 * @param fType
	 *            格式化类型
	 * @return
	 */
	public static String decimalFormatDouble(double dfValue) {
		try {
			DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
			if (dfValue > 0) {
				return decimalFormat.format(dfValue);
			}
		} catch (Exception e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}
		return "";
	}

	/**
	 * 格式化数字.
	 * 
	 * @param dfValue
	 *            格式化数字
	 * @param fType
	 *            格式化类型
	 * @return
	 */
	public static String decimalFormatDouble(double dfValue, int fType) {
		try {
			String formatType = "0";
			if (fType == number2) {
				formatType = "#,##0.00";
			} else if (fType == number3) {
				formatType = "###0.00";
			}
			DecimalFormat decimalFormat = new DecimalFormat(formatType);
			if (dfValue > 0) {
				return decimalFormat.format(dfValue);
			}
		} catch (Exception e) {
			logger.error(StackTraceUtil.getStackTrace(e));
		}
		return "";
	}

	/**
	 * 判断字符串是否全部为字母.
	 * 
	 * @return
	 */
	public static boolean isStr(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (!(s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
					&& !(s.charAt(i) >= 'a' && s.charAt(i) <= 'z')) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 首字母转小写.
	 * 
	 * @return String
	 */
	public static String toLowerCaseFirstOne(String s) {
		if (Character.isLowerCase(s.charAt(0))) {
			return s;
		} else {
			return (new StringBuilder())
					.append(Character.toLowerCase(s.charAt(0)))
					.append(s.substring(1)).toString();
		}
	}

	/**
	 * 首字母转大写.
	 * 
	 * @return String
	 */
	public static String toUpperCaseFirstOne(String s) {
		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		} else {
			return (new StringBuilder())
					.append(Character.toUpperCase(s.charAt(0)))
					.append(s.substring(1)).toString();
		}
	}
	
	/**
	 * toFillZerosBeforeNum:在数字前面补0. <br/>
	 *
	 * @author shuxinwu
	 * @param digitNum 位数
	 * @param num 需要补0的数
	 * @return
	 */
	public static String toFillZerosBeforeNum(int digitNum, int num) {
	    String str = String.format("%0" + digitNum + "d", num);  
	    return str;
	}
	
	/** 
	 * @Title: checkProvince 
	 * @Description: 判断province是否是直辖市.
	 * @Auth shuxinwu
	 * @Date 2016年6月12日 下午3:34:35
	 * @param province
	 * @return
	 * @throws 
	 */
	public static boolean checkProvince(String province) {
		if (BEIJING.equals(province) ||
				TIANJIN.equals(province) ||
				SHANGHAI.equals(province) ||
				CHONGQIN.equals(province) ||
				SHENZHEN.equals(province)) {
			return true;
		}
		return false;
	}
	
}