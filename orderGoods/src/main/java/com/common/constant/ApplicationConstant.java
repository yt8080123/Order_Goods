package com.common.constant;


/**
 * Class Name: ApplicationConstant
 * Description: application level constants.
 * 
 * @author SC
 * 
 */
public final class ApplicationConstant {

    public static final String SESSIONUSER = "user";

    public static final String GLOBAL_CONTEXT = "/web/";

    public static final String X_FORM_ID = "x-form-id";

    public static final String ARRARY_REGEXP = "^([a-zA-Z0-9_]+)(\\[[0-9]+\\])$";

    public static final String MANUAL_VALIDATE = "manualValidate";

    // Log discriminator
    public static final String LOG_USER_NAME = "userName";
    
    public static final String LOG_SESSION_ID = "sessionId";
    //初始密码
    public static final String DEFAULT_PASS_WORD = "888888";
    //移动端表单名用来控制回报错误
    public static final String MOB_FORM_ID = "mob-form";
    
    public static final String DEFAULT_STATUSSTR = "A";
    
    public static final String SHIBOR_URL = "http://www.shibor.org/shibor/web/html/shibor.html";
    
    public static final String GET = "GET";
    
    public static final String POST = "POST";
    //超级管理员用户名
    public static final String ADMIN_USER = "admin";
    
    public static final String IMAGE_PATH = "/web/common/image?fileName=";
    //标题图片宽度
    public static final int TITLE_ICO_WIDTH = 1024;
    //标题小图片宽度
    public static final int TITLE_ICO_WIDTH_MIN = 100;
    //标题中图片宽度
    public static final int TITLE_ICO_WIDTH_MID = 220;
    //开发环境
    public static final String ENV_DEV = "DEV";
    //生产环境
    public static final String ENV_PROD = "PROD";
    //new UAT
    public static final String ENV_NUAT = "NUAT";
    //恒天UAT环境
    public static final String ENV_UAT = "UAT";
    //子机构帐号
    public static final String SUB_ACCOUNT = "sub";
    //主机构账号
    public static final String MAIN_ACCOUNT = "main";
    //账号
    public static final String ACCOUNT = "account";
    //非账号
    public static final String NOT_ACCOUNT = "notAccount";
    
    //短链接跳转私钥
    public static final String SELF_KEY = "QCAPP_KEY";

    public static final int sendTime = 120000;
    
    private ApplicationConstant() {

    }

}
